FROM openjdk:8
ADD target/boot-mydocker.jar boot-mydocker.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "boot-mydocker.jar"]