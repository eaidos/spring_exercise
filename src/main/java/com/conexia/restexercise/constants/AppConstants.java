package com.conexia.restexercise.constants;


public class AppConstants {

    public static final String GENERAL_ERROR = "General internal server error.";
    public static final String SUCCESS = "SUCCESS";
    public static final String ERROR = "ERROR";
    public static final String NOT_FOUND = "NO DATA";

    public static final String WEATHER_SERVICE_CURRENT_PATH = "current";
    public static final String WEATHER_SERVICE_SUCCESS_MESSAGE = "Weather retrieved successfully.";
    public static final String WEATHER_SERVICE_UNREACHABLE = "External weather service API not available or network error.";
    public static final String WEATHER_SERVICE_NO_DATA = "No data found.";

    public static final String FIND_ALL_CITIES_SUCCESS_MESSAGE = "Cities retrieved successfully.";
    public static final String FIND_ALL_CITIES_ERROR_MESSAGE = "Could not retrieve any city due to an exception.";
    public static final String FIND_ALL_CITIES_EMPTY_MESSAGE = "There are no cities in the database with that property.";
    public static final String FIND_CITY_SUCCESS_MESSAGE = "City retrieved successfully.";
    public static final String FIND_CITY_ERROR_MESSAGE = "Could not retrieve any city due to an exception.";
    public static final String FIND_CITY_EMPTY_MESSAGE = "There isn't any city in the database with that property.";
    public static final String UPDATE_CITY_SUCCESS_MESSAGE = "City updated successfully.";
    public static final String UPDATE_CITY_ERROR_MESSAGE = "Could not update city due to an exception.";
    public static final String DELETE_CITY_SUCCESS_MESSAGE = "City deleted successfully.";
    public static final String DELETE_CITY_ERROR_MESSAGE = "Could not delete city due to an exception.";
    public static final String DELETE_CITY_EMPTY_MESSAGE = "There isn't any city in the database with that property.";
    public static final String CREATE_CITY_ERROR_MESSAGE = "Could not create city due to an exception.";
    public static final String CREATE_CITY_SUCCESS_MESSAGE = "City created successfully.";


    public static final String FIND_ALL_COUNTRIES_ERROR_MESSAGE ="Could not retrieve any country due to an exception.";
    public static final String FIND_ALL_COUNTRIES_SUCCESS_MESSAGE="Countries retrieved successfully.";
    public static final String FIND_ALL_COUNTRIES_EMPTY_MESSAGE="There are no countries in the database with that property.";
    public static final String FIND_COUNTRY_SUCCESS_MESSAGE = "Country retrieved successfully.";
    public static final String FIND_COUNTRY_ERROR_MESSAGE = "Could not retrieve any country due to an exception.";
    public static final String FIND_COUNTRY_EMPTY_MESSAGE = "There isn't any country in the database with that property.";
    public static final String UPDATE_COUNTRY_SUCCESS_MESSAGE = "Country updated successfully.";
    public static final String UPDATE_COUNTRY_ERROR_MESSAGE = "Could not update country due to an exception.";
    public static final String DELETE_COUNTRY_SUCCESS_MESSAGE = "Country deleted successfully.";
    public static final String DELETE_COUNTRY_ERROR_MESSAGE = "Could not delete country due to an exception.";
    public static final String DELETE_COUNTRY_CITIES_LINKED_ERROR_MESSAGE = "Could not delete country because there are"+
                                                                            " cities still linked to it.";
    public static final String DELETE_COUNTRY_STATIONS_LINKED_ERROR_MESSAGE = "Could not delete country because there are"+
                                                                              " weather stations still linked to it.";
    public static final String CREATE_COUNTRY_ERROR_MESSAGE = "Could not create country due to an exception.";
    public static final String CREATE_COUNTRY_SUCCESS_MESSAGE = "Country created successfully.";

    public static final String FIND_ALL_WEATHER_STATION_SUCCESS_MESSAGE = "Weather stations retrieved successfully.";
    public static final String FIND_ALL_WEATHER_STATION_ERROR_MESSAGE = "Could not retrieve any weather stations due to an exception.";
    public static final String FIND_ALL_WEATHER_STATION_EMPTY_MESSAGE = "There are no weather stations in the database with that property.";
    public static final String FIND_WEATHER_STATION_SUCCESS_MESSAGE = "Weather station retrieved successfully.";
    public static final String FIND_WEATHER_STATION_ERROR_MESSAGE = "Could not retrieve any weather station due to an exception.";
    public static final String FIND_WEATHER_STATION_EMPTY_MESSAGE = "There isn't any weather station in the database with that property.";
    public static final String UPDATE_WEATHER_STATION_SUCCESS_MESSAGE = "Weather station updated successfully.";
    public static final String UPDATE_WEATHER_STATION_ERROR_MESSAGE = "Could not update weather station due to an exception.";
    public static final String DELETE_WEATHER_STATION_SUCCESS_MESSAGE = "Weather station deleted successfully.";
    public static final String DELETE_WEATHER_STATION_ERROR_MESSAGE = "Could not weather station city due to an exception.";
    public static final String CREATE_WEATHER_STATION_ERROR_MESSAGE = "Could not create weather station due to an exception.";
    public static final String CREATE_WEATHER_STATION_SUCCESS_MESSAGE = "Weather station created successfully.";

    public static final boolean INCLUDE_DELETED = true;
    public static final boolean NOT_INCLUDE_DELETED = false;


}
