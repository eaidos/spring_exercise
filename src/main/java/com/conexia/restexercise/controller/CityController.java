package com.conexia.restexercise.controller;

import com.conexia.restexercise.constants.AppConstants;
import com.conexia.restexercise.domain.City;
import com.conexia.restexercise.domain.Coordinates;
import com.conexia.restexercise.domain.Country;
import com.conexia.restexercise.dto.internal.request.CityRequestDTO;
import com.conexia.restexercise.dto.internal.response.CityResponseDTO;
import com.conexia.restexercise.exception.CityNotFoundException;
import com.conexia.restexercise.exception.CountryNotFoundException;
import com.conexia.restexercise.service.CityService;
import com.conexia.restexercise.service.CountryService;
import lombok.val;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/city")
public class CityController {

    @Autowired
    CountryService countryService;

    @Autowired
    CityService cityService;

    @GetMapping(value = "/")
    ResponseEntity<CityResponseDTO> getAllCities() {

        val cityResponse = new CityResponseDTO();
        List<City> cityList;

        try {
            cityList = this.cityService.getAllCities();
            cityResponse.setData(cityList);
            cityResponse.setStatus(AppConstants.SUCCESS);
        } catch (Exception e) {
            cityResponse.setStatus(AppConstants.ERROR);
            cityResponse.setMessage(AppConstants.FIND_ALL_CITIES_ERROR_MESSAGE);
            return new ResponseEntity<>(cityResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (cityList.size() == 0) {
            cityResponse.setStatus(AppConstants.ERROR);
            cityResponse.setMessage(AppConstants.FIND_ALL_CITIES_EMPTY_MESSAGE);
            return new ResponseEntity<>(cityResponse, HttpStatus.NOT_FOUND);
        }

        cityResponse.setMessage(AppConstants.FIND_ALL_CITIES_SUCCESS_MESSAGE);
        return new ResponseEntity<>(cityResponse, HttpStatus.OK);
    }

    @GetMapping(value = "/{cityId:\\d+}")
    ResponseEntity<CityResponseDTO> getCityById(@PathVariable Long cityId) {
        val cityResponse = new CityResponseDTO();
        val cityList = new ArrayList<City>();
        Optional<City> city;
        cityResponse.setData(cityList);

        try {
            city = this.cityService.getCityById(cityId);
            if (city.isPresent()) {
                cityList.add(city.get());
                cityResponse.setStatus(AppConstants.SUCCESS);
            } else {
                throw new CityNotFoundException();
            }
        } catch (CityNotFoundException e) {
            cityResponse.setStatus(AppConstants.ERROR);
            cityResponse.setMessage(AppConstants.FIND_CITY_EMPTY_MESSAGE);
            return new ResponseEntity<>(cityResponse, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            cityResponse.setStatus(AppConstants.ERROR);
            cityResponse.setMessage(AppConstants.FIND_CITY_ERROR_MESSAGE);
            return new ResponseEntity<>(cityResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        cityResponse.setMessage(AppConstants.FIND_CITY_SUCCESS_MESSAGE);
        return new ResponseEntity<>(cityResponse, HttpStatus.OK);
    }

    @GetMapping(value = "/country/{countryId:\\d+}")
    ResponseEntity<CityResponseDTO> getAllCitiesByCountryId(@PathVariable Long countryId) {

        val cityResponse = new CityResponseDTO();
        Optional<Country> optCountry;
        List<City> cityList;
        cityResponse.setData(new ArrayList<City>());

        try {
            optCountry = this.countryService.getCountryById(countryId);
            if (optCountry.isPresent()){
                cityList = this.cityService.getAllCitiesByCountryId(countryId);
                cityResponse.setData(cityList);
                if (cityList != null && cityList.isEmpty()) {
                    throw new CityNotFoundException();
                }
            } else {
                throw new CountryNotFoundException();
            }
        } catch (CityNotFoundException e) {
            cityResponse.setStatus(AppConstants.ERROR);
            cityResponse.setMessage(AppConstants.FIND_ALL_CITIES_EMPTY_MESSAGE);
            return new ResponseEntity<>(cityResponse, HttpStatus.NOT_FOUND);
        } catch (CountryNotFoundException e) {
            cityResponse.setStatus(AppConstants.ERROR);
            cityResponse.setMessage(AppConstants.FIND_COUNTRY_EMPTY_MESSAGE);
            return new ResponseEntity<>(cityResponse, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            cityResponse.setStatus(AppConstants.ERROR);
            cityResponse.setMessage(AppConstants.FIND_ALL_CITIES_ERROR_MESSAGE);
            return new ResponseEntity<>(cityResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        cityResponse.setStatus(AppConstants.SUCCESS);
        cityResponse.setMessage(AppConstants.FIND_ALL_CITIES_SUCCESS_MESSAGE);
        return new ResponseEntity<>(cityResponse, HttpStatus.OK);
    }

    @GetMapping(value = "/country/{countryCode:[A-Z]+}")
    ResponseEntity<CityResponseDTO> getAllCitiesByCountryCode(@PathVariable String countryCode) {

        val cityResponse = new CityResponseDTO();
        List<City> cityList;
        Optional<Country> optCountry;
        cityResponse.setData(new ArrayList<City>());

        try {
            optCountry = this.countryService.getCountryByCode(countryCode,AppConstants.NOT_INCLUDE_DELETED);
            if (optCountry.isPresent()){
                cityList = this.cityService.getAllCitiesByCountryCode(countryCode, optCountry.get().getId());
                cityResponse.setData(cityList);
                if (cityList != null && cityList.isEmpty()) {
                    throw new CityNotFoundException();
                }
            } else {
                throw new CountryNotFoundException();
            }
        } catch (CityNotFoundException e) {
            cityResponse.setStatus(AppConstants.ERROR);
            cityResponse.setMessage(AppConstants.FIND_ALL_CITIES_EMPTY_MESSAGE);
            return new ResponseEntity<>(cityResponse, HttpStatus.NOT_FOUND);
        } catch (CountryNotFoundException e) {
            cityResponse.setStatus(AppConstants.ERROR);
            cityResponse.setMessage(AppConstants.FIND_COUNTRY_EMPTY_MESSAGE);
            return new ResponseEntity<>(cityResponse, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            cityResponse.setStatus(AppConstants.ERROR);
            cityResponse.setMessage(AppConstants.FIND_ALL_CITIES_ERROR_MESSAGE);
            return new ResponseEntity<>(cityResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        cityResponse.setStatus(AppConstants.SUCCESS);
        cityResponse.setMessage(AppConstants.FIND_ALL_CITIES_SUCCESS_MESSAGE);
        return new ResponseEntity<>(cityResponse, HttpStatus.OK);
    }

    @PostMapping(value = "/")
    ResponseEntity<CityResponseDTO> createCity(@RequestBody CityRequestDTO request) {

        val cityResponse = new CityResponseDTO();
        val cityList = new ArrayList<City>();
        Optional<Country> optCountry;
        City city;

        cityResponse.setData(cityList);

        try {
            optCountry = this.countryService.getCountryById(request.getCountryId());
            if (optCountry.isPresent()){
                    city = createNewCity(request,optCountry.get());
                    cityList.add(city);
                    cityResponse.setStatus(AppConstants.SUCCESS);
            } else {
                throw new CountryNotFoundException();
            }
        } catch (CountryNotFoundException e) {
            cityResponse.setStatus(AppConstants.ERROR);
            cityResponse.setMessage(AppConstants.FIND_COUNTRY_EMPTY_MESSAGE);
            return new ResponseEntity<>(cityResponse, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            cityResponse.setStatus(AppConstants.ERROR);
            cityResponse.setMessage(AppConstants.CREATE_CITY_ERROR_MESSAGE);
            return new ResponseEntity<>(cityResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        cityResponse.setMessage(AppConstants.CREATE_CITY_SUCCESS_MESSAGE);
        return new ResponseEntity<>(cityResponse, HttpStatus.OK);

    }

    @PutMapping(value = "/{cityId:\\d+}")
    ResponseEntity<CityResponseDTO> modifyCityById(@PathVariable Long cityId, @RequestBody CityRequestDTO request)
    {
        val cityResponse = new CityResponseDTO();
        val cityList = new ArrayList<City>();
        Optional<City> optCity;
        Optional<Country> optCountry;
        var city = new City();

        cityResponse.setData(cityList);

        try {
            optCountry = this.countryService.getCountryById(request.getCountryId());
            if (optCountry.isPresent()){
                optCity = this.cityService.getCityById(cityId);
                if (optCity.isPresent()) {
                    city = modifyCity(optCity,request,optCountry.get());
                    cityList.add(city);
                    cityResponse.setStatus(AppConstants.SUCCESS);
                } else {
                    throw new CityNotFoundException();
                }
            } else {
                throw new CountryNotFoundException();
            }
        } catch (CountryNotFoundException e) {
            cityResponse.setStatus(AppConstants.ERROR);
            cityResponse.setMessage(AppConstants.FIND_COUNTRY_EMPTY_MESSAGE);
            return new ResponseEntity<>(cityResponse, HttpStatus.NOT_FOUND);
        } catch (CityNotFoundException e) {
            cityResponse.setStatus(AppConstants.ERROR);
            cityResponse.setMessage(AppConstants.FIND_CITY_EMPTY_MESSAGE);
            return new ResponseEntity<>(cityResponse, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            cityResponse.setStatus(AppConstants.ERROR);
            cityResponse.setMessage(AppConstants.UPDATE_CITY_ERROR_MESSAGE);
            return new ResponseEntity<>(cityResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        cityResponse.setMessage(AppConstants.UPDATE_CITY_SUCCESS_MESSAGE);
        return new ResponseEntity<>(cityResponse, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{cityId:\\d+}")
    ResponseEntity<CityResponseDTO> deleteCityById(@PathVariable Long cityId)
    {
        val cityResponse = new CityResponseDTO();
        var cityList = new ArrayList<City>();
        Optional<City> optCity;
        var city = new City();

        cityResponse.setData(cityList);

        try {
            optCity = this.cityService.getCityById(cityId);

            if (optCity.isPresent()) {
                city = deleteCity(optCity);
                cityList.add(city);
                cityResponse.setStatus(AppConstants.SUCCESS);
            } else {
                throw new CityNotFoundException();
            }
        } catch (CityNotFoundException e) {
            cityResponse.setStatus(AppConstants.ERROR);
            cityResponse.setMessage(AppConstants.DELETE_CITY_EMPTY_MESSAGE);
            return new ResponseEntity<>(cityResponse, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            cityResponse.setStatus(AppConstants.ERROR);
            cityResponse.setMessage(AppConstants.DELETE_CITY_ERROR_MESSAGE);
            return new ResponseEntity<>(cityResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        cityResponse.setMessage(AppConstants.DELETE_CITY_SUCCESS_MESSAGE);
        return new ResponseEntity<>(cityResponse, HttpStatus.OK);
    }


    private City modifyCity(Optional<City> optCity, CityRequestDTO request, Country country) {
        val city = optCity.get();
        val coords = request.getCoordinates();
        city.setCityName(request.getCityName());
        city.setDateUpdated(LocalDate.now());
        city.setCountry(country);
        city.setCoordinates(new Coordinates(coords.getLatitude(),
                                            coords.getLongitude())
                           );
        this.cityService.updateCity(city);
        return city;
    }

    private City createNewCity(CityRequestDTO request, Country country) {
        var city = new City();
        val coords = request.getCoordinates();
        city.setCityName(request.getCityName());
        city.setCountry(country);
        city.setCoordinates(new Coordinates(coords.getLatitude(),
                                            coords.getLongitude())
        );
        city.setDateCreated(LocalDate.now());
        city.setDeleted(false);
        return this.cityService.createCity(city);

    }

    private City deleteCity(Optional<City> optCity) {
        val city = optCity.get();
        city.setDeleted(true);
        city.setDateDeleted(LocalDate.now());
        this.cityService.updateCity(city);
        return city;
    }
}