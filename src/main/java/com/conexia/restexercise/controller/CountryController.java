package com.conexia.restexercise.controller;

import com.conexia.restexercise.constants.AppConstants;
import com.conexia.restexercise.domain.Country;
import com.conexia.restexercise.dto.internal.request.CountryRequestDTO;
import com.conexia.restexercise.dto.internal.response.CountryResponseDTO;
import com.conexia.restexercise.exception.CitiesStillLinkedToCountryException;
import com.conexia.restexercise.exception.CountryNotFoundException;
import com.conexia.restexercise.exception.StationsStillLinkedToCountryException;
import com.conexia.restexercise.service.CityService;
import com.conexia.restexercise.service.CountryService;
import com.conexia.restexercise.service.WeatherStationService;
import lombok.val;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/country")
public class CountryController {

    @Autowired
    CountryService countryService;

    @Autowired
    CityService cityService;

    @Autowired
    WeatherStationService weatherStationService;

    @GetMapping(value = "/")
    ResponseEntity<CountryResponseDTO> getAllCountries() {

        val countryResponse = new CountryResponseDTO();
        List<Country> countryList = new ArrayList<Country>();
        countryResponse.setData(countryList);

        try {
            countryList = this.countryService.getAllCountries();
            countryResponse.setData(countryList);
            if (countryList.size() == 0) {
                throw new CountryNotFoundException();
            }
        } catch (CountryNotFoundException e) {
            countryResponse.setStatus(AppConstants.ERROR);
            countryResponse.setMessage(AppConstants.FIND_ALL_COUNTRIES_EMPTY_MESSAGE);
            return new ResponseEntity<>(countryResponse, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            countryResponse.setStatus(AppConstants.ERROR);
            countryResponse.setMessage(AppConstants.FIND_ALL_COUNTRIES_ERROR_MESSAGE);
            return new ResponseEntity<>(countryResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        countryResponse.setStatus(AppConstants.SUCCESS);
        countryResponse.setMessage(AppConstants.FIND_ALL_COUNTRIES_SUCCESS_MESSAGE);
        return new ResponseEntity<>(countryResponse, HttpStatus.OK);
    }

    @GetMapping(value = "/{countryId:\\d+}")
    ResponseEntity<CountryResponseDTO> getCountryById(@PathVariable Long countryId) {

        val countryResponse = new CountryResponseDTO();
        val countryList = new ArrayList<Country>();
        Optional<Country> country;
        countryResponse.setData(countryList);

        try {
            country = this.countryService.getCountryById(countryId);
            if (country.isPresent()) {
                countryList.add(country.get());
                countryResponse.setStatus(AppConstants.SUCCESS);
            } else {
                throw new CountryNotFoundException();
            }
        } catch (CountryNotFoundException e) {
            countryResponse.setStatus(AppConstants.ERROR);
            countryResponse.setMessage(AppConstants.FIND_COUNTRY_EMPTY_MESSAGE);
            return new ResponseEntity<>(countryResponse, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            countryResponse.setStatus(AppConstants.ERROR);
            countryResponse.setMessage(AppConstants.FIND_COUNTRY_ERROR_MESSAGE);
            return new ResponseEntity<>(countryResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        countryResponse.setMessage(AppConstants.FIND_COUNTRY_SUCCESS_MESSAGE);
        return new ResponseEntity<>(countryResponse, HttpStatus.OK);
    }

    @GetMapping(value = "/{countryCode:[A-Z]+}")
    ResponseEntity<CountryResponseDTO> getCountryByCode(@PathVariable String countryCode) {

        val countryResponse = new CountryResponseDTO();
        val countryList = new ArrayList<Country>();
        Optional<Country> country;
        countryResponse.setData(countryList);


        try {
            country = this.countryService.getCountryByCode(countryCode, AppConstants.NOT_INCLUDE_DELETED);

            if (country.isPresent()) {
                countryList.add(country.get());
                countryResponse.setData(countryList);
                countryResponse.setStatus(AppConstants.SUCCESS);
            } else {
                throw new CountryNotFoundException();
            }
        } catch (CountryNotFoundException e) {
            countryResponse.setStatus(AppConstants.ERROR);
            countryResponse.setMessage(AppConstants.FIND_COUNTRY_EMPTY_MESSAGE);
            return new ResponseEntity<>(countryResponse, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            countryResponse.setStatus(AppConstants.ERROR);
            countryResponse.setMessage(AppConstants.FIND_COUNTRY_ERROR_MESSAGE);
            return new ResponseEntity<>(countryResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        countryResponse.setMessage(AppConstants.FIND_COUNTRY_SUCCESS_MESSAGE);
        return new ResponseEntity<>(countryResponse, HttpStatus.OK);
    }

    @PostMapping(value = "/")
    ResponseEntity<CountryResponseDTO> createCountry(@RequestBody CountryRequestDTO request) {

        val countryResponse = new CountryResponseDTO();
        val countryList = new ArrayList<Country>();
        Optional<Country> optCountry;
        Country country;
        countryResponse.setData(countryList);

        try {
            optCountry = this.countryService.getCountryByCode(request.getCode(), AppConstants.NOT_INCLUDE_DELETED);
            if (!optCountry.isPresent()) {
                country = createNewCountry(request);
                countryList.add(country);
                countryResponse.setStatus(AppConstants.SUCCESS);
            } else {
                country = undeleteAndModifyCountry(optCountry, request);
                countryList.add(country);
                countryResponse.setStatus(AppConstants.SUCCESS);
            }
        } catch (Exception e) {
            countryResponse.setStatus(AppConstants.ERROR);
            countryResponse.setMessage(AppConstants.CREATE_COUNTRY_ERROR_MESSAGE);
            return new ResponseEntity<>(countryResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        countryResponse.setMessage(AppConstants.CREATE_COUNTRY_SUCCESS_MESSAGE);
        return new ResponseEntity<>(countryResponse, HttpStatus.OK);

    }


    @PutMapping(value = "/{countryCode:[A-Z]+}")
    ResponseEntity<CountryResponseDTO> modifyCountryByCode(@PathVariable String countryCode,
                                                           @RequestBody CountryRequestDTO request)
    {
        val countryResponse = new CountryResponseDTO();
        val countryList = new ArrayList<Country>();
        Optional<Country> optCountry;
        var country = new Country();
        countryResponse.setData(countryList);

        try {
            optCountry = this.countryService.getCountryByCode(countryCode, AppConstants.NOT_INCLUDE_DELETED);

            if (optCountry.isPresent()) {
                country = modifyCountry(optCountry,request);
                countryList.add(country);
                countryResponse.setData(countryList);
                countryResponse.setStatus(AppConstants.SUCCESS);
            } else {
                throw new CountryNotFoundException();
            }
        } catch (CountryNotFoundException e) {
            countryResponse.setStatus(AppConstants.ERROR);
            countryResponse.setMessage(AppConstants.FIND_COUNTRY_EMPTY_MESSAGE);
            return new ResponseEntity<>(countryResponse, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            countryResponse.setStatus(AppConstants.ERROR);
            countryResponse.setMessage(AppConstants.UPDATE_COUNTRY_ERROR_MESSAGE);
            return new ResponseEntity<>(countryResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        countryResponse.setMessage(AppConstants.UPDATE_COUNTRY_SUCCESS_MESSAGE);
        return new ResponseEntity<>(countryResponse, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{countryCode:[A-Z]+}")
    ResponseEntity<CountryResponseDTO> deleteCountryByCode(@PathVariable String countryCode)
    {
        val countryResponse = new CountryResponseDTO();
        val countryList = new ArrayList<Country>();
        Optional<Country> optCountry;
        var country = new Country();
        countryResponse.setData(countryList);

        try {
            optCountry = this.countryService.getCountryByCode(countryCode, AppConstants.NOT_INCLUDE_DELETED);

            if (optCountry.isPresent()) {
                if (areAvailableCitiesForCountry(countryCode, optCountry.get().getId())){
                    throw new CitiesStillLinkedToCountryException();
                }
                if (areAvailableWeatherStationsForCountry(countryCode)){
                    throw new StationsStillLinkedToCountryException();
                }
                country = deleteCountry(optCountry);
                countryList.add(country);
                countryResponse.setData(countryList);
                countryResponse.setStatus(AppConstants.SUCCESS);
            } else {
                throw new CountryNotFoundException();
            }
        } catch (CountryNotFoundException e) {
            countryResponse.setStatus(AppConstants.ERROR);
            countryResponse.setMessage(AppConstants.FIND_COUNTRY_EMPTY_MESSAGE);
            return new ResponseEntity<>(countryResponse, HttpStatus.NOT_FOUND);
        } catch (CitiesStillLinkedToCountryException e) {
            countryResponse.setStatus(AppConstants.ERROR);
            countryResponse.setMessage(AppConstants.DELETE_COUNTRY_CITIES_LINKED_ERROR_MESSAGE);
            return new ResponseEntity<>(countryResponse, HttpStatus.FORBIDDEN);
        }  catch (StationsStillLinkedToCountryException e) {
            countryResponse.setStatus(AppConstants.ERROR);
            countryResponse.setMessage(AppConstants.DELETE_COUNTRY_STATIONS_LINKED_ERROR_MESSAGE);
            return new ResponseEntity<>(countryResponse, HttpStatus.FORBIDDEN);
        } catch (Exception e) {
            countryResponse.setStatus(AppConstants.ERROR);
            countryResponse.setMessage(AppConstants.DELETE_COUNTRY_ERROR_MESSAGE);
            return new ResponseEntity<>(countryResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        countryResponse.setMessage(AppConstants.DELETE_COUNTRY_SUCCESS_MESSAGE);
        return new ResponseEntity<>(countryResponse, HttpStatus.OK);
    }


    private Country createNewCountry(CountryRequestDTO request) {
        var country = new Country();
        country.setDescription(request.getDescription());
        country.setCode(request.getCode());
        country.setDateCreated(LocalDate.now());
        country.setDeleted(false);
        return this.countryService.createCountry(country);
    }

    private Country modifyCountry(Optional<Country> optCountry, CountryRequestDTO request) {
        val country = optCountry.get();
        country.setDescription(request.getDescription());
        country.setDateUpdated(LocalDate.now());
        return this.countryService.updateCountry(country);
    }

    private Country undeleteAndModifyCountry(Optional<Country> optCountry, CountryRequestDTO request) {
        val country = optCountry.get();
        country.setDescription(request.getDescription());
        country.setDeleted(false);
        country.setDateDeleted(null);
        country.setDateUpdated(null);
        country.setDateCreated(LocalDate.now());
        return this.countryService.updateCountry(country);
    }

    private Country deleteCountry(Optional<Country> optCountry) {
        val country = optCountry.get();
        country.setDeleted(true);
        country.setDateDeleted(LocalDate.now());
        this.countryService.updateCountry(country);
        return country;
    }

    private boolean areAvailableCitiesForCountry(String countryCode, long countryId){
        val cityList = cityService.getAllCitiesByCountryCode(countryCode, countryId);
        if(cityList != null && cityList.size() > 0) {
            return true;
        }
        return false;
    }

    private boolean areAvailableWeatherStationsForCountry(String countryCode){
        val stationList = weatherStationService.getAllStationsByCountryCode(countryCode);
        if(stationList != null && stationList.size() > 0) {
            return true;
        }
        return false;
    }
}
