package com.conexia.restexercise.controller;

import com.conexia.restexercise.constants.AppConstants;
import com.conexia.restexercise.converter.WeatherDTOConverter;
import com.conexia.restexercise.domain.WeatherStation;
import com.conexia.restexercise.dto.external.request.WeatherByCoordinatesRequestDTO;
import com.conexia.restexercise.dto.external.request.WeatherByPostalRequestDTO;
import com.conexia.restexercise.dto.external.request.WeatherByStationRequestDTO;
import com.conexia.restexercise.dto.external.response.DailyWeatherResponseDTO;
import com.conexia.restexercise.dto.internal.response.WeatherResponseDTO;
import com.conexia.restexercise.exception.ExtWeatherServiceError;
import com.conexia.restexercise.exception.ExtWeatherServiceNoData;
import com.conexia.restexercise.exception.ExtWeatherServiceNotAvailable;
import com.conexia.restexercise.exception.WeatherStationNotFoundException;
import com.conexia.restexercise.service.WeatherService;
import com.conexia.restexercise.service.WeatherStationService;
import lombok.val;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Optional;

@RestController
@RequestMapping("/weather")
public class WeatherController {

    @Autowired
    WeatherService weatherService;

    @Autowired
    WeatherStationService weatherStationService;

    @GetMapping(value = "/current/{latitude}/{longitude}")
    ResponseEntity<WeatherResponseDTO> getWeatherByCoordinates(@PathVariable String latitude,
                                                               @PathVariable String longitude) {

        val weatherCoordExternalReq = new WeatherByCoordinatesRequestDTO(latitude, longitude);
        var weatherResponse = new WeatherResponseDTO();
        var weatherResponseFromService = new DailyWeatherResponseDTO();

        weatherResponse.setData(new ArrayList<>());
        try {
            weatherResponseFromService = this.weatherService.getCurrentWeatherByCoordinates(weatherCoordExternalReq);
            weatherResponse = WeatherDTOConverter.fromExternalToInternalDTOResponse(weatherResponseFromService,
                                                                                    weatherResponse);
        } catch (ExtWeatherServiceNoData e) {
            weatherResponse.setStatus(AppConstants.NOT_FOUND);
            weatherResponse.setMessage(AppConstants.WEATHER_SERVICE_NO_DATA);
            return new ResponseEntity<>(weatherResponse, HttpStatus.NOT_FOUND);
        } catch (ExtWeatherServiceError e) {
            weatherResponse.setStatus(AppConstants.ERROR);
            weatherResponse.setMessage(e.getErrorMessage());
            return new ResponseEntity<>(weatherResponse, HttpStatus.valueOf(e.getStatusCode()));
        } catch (ExtWeatherServiceNotAvailable e) {
            weatherResponse.setStatus(AppConstants.ERROR);
            weatherResponse.setMessage(AppConstants.GENERAL_ERROR);
            return new ResponseEntity<>(weatherResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            weatherResponse.setStatus(AppConstants.ERROR);
            weatherResponse.setMessage(AppConstants.GENERAL_ERROR);
            return new ResponseEntity<>(weatherResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        weatherResponse.setStatus(AppConstants.SUCCESS);
        weatherResponse.setMessage(AppConstants.WEATHER_SERVICE_SUCCESS_MESSAGE);
        return new ResponseEntity<>(weatherResponse, HttpStatus.OK);
    }

    @GetMapping(value = "/current/station/{stationCode:[A-Z]+}")
    ResponseEntity<WeatherResponseDTO> getWeatherByStation(@PathVariable String stationCode) {

        val weatherStationExternalReq = new WeatherByStationRequestDTO(stationCode);
        var weatherResponse = new WeatherResponseDTO();
        var weatherResponseFromService = new DailyWeatherResponseDTO();
        weatherResponse.setData(new ArrayList<>());
        Optional<WeatherStation> optStation;

        try {
            optStation = this.weatherStationService.getWeatherStationByCode(stationCode);
            if(optStation.isPresent()) {
                weatherResponseFromService = this.weatherService.getCurrentWeatherByStation(weatherStationExternalReq);
                weatherResponse = WeatherDTOConverter.fromExternalToInternalDTOResponse(weatherResponseFromService,
                                                                                        weatherResponse);
            } else {
                throw new WeatherStationNotFoundException();
            }
        } catch (WeatherStationNotFoundException e) {
            weatherResponse.setStatus(AppConstants.ERROR);
            weatherResponse.setMessage(AppConstants.FIND_ALL_WEATHER_STATION_EMPTY_MESSAGE);
            return new ResponseEntity<>(weatherResponse, HttpStatus.NOT_FOUND);
        } catch (ExtWeatherServiceNoData e) {
            weatherResponse.setStatus(AppConstants.NOT_FOUND);
            weatherResponse.setMessage(AppConstants.WEATHER_SERVICE_NO_DATA);
            return new ResponseEntity<>(weatherResponse, HttpStatus.NOT_FOUND);
        } catch (ExtWeatherServiceError e) {
            weatherResponse.setStatus(AppConstants.ERROR);
            weatherResponse.setMessage(e.getMessage());
            return new ResponseEntity<>(weatherResponse, HttpStatus.NOT_FOUND);
        } catch (ExtWeatherServiceNotAvailable e) {
            weatherResponse.setStatus(AppConstants.ERROR);
            weatherResponse.setMessage(AppConstants.GENERAL_ERROR);
            return new ResponseEntity<>(weatherResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            weatherResponse.setStatus(AppConstants.ERROR);
            weatherResponse.setMessage(AppConstants.GENERAL_ERROR);
            return new ResponseEntity<>(weatherResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        weatherResponse.setStatus(AppConstants.SUCCESS);
        weatherResponse.setMessage(AppConstants.WEATHER_SERVICE_SUCCESS_MESSAGE);
        return new ResponseEntity<>(weatherResponse, HttpStatus.OK);
    }

    @GetMapping(value = "/current/{postalCode}")
    ResponseEntity<WeatherResponseDTO> getWeatherByPostalCode(@PathVariable String postalCode) {

        val weatherStationExternalReq = new WeatherByPostalRequestDTO(postalCode);
        var weatherResponse = new WeatherResponseDTO();
        var weatherResponseFromService = new DailyWeatherResponseDTO();
        weatherResponse.setData(new ArrayList<>());

        try {
            weatherResponseFromService = this.weatherService.getCurrentWeatherByPostalCode(weatherStationExternalReq);
            weatherResponse = WeatherDTOConverter.fromExternalToInternalDTOResponse(weatherResponseFromService, weatherResponse);
        } catch (ExtWeatherServiceNoData e) {
            weatherResponse.setStatus(AppConstants.NOT_FOUND);
            weatherResponse.setMessage(AppConstants.WEATHER_SERVICE_NO_DATA);
            return new ResponseEntity<>(weatherResponse, HttpStatus.NOT_FOUND);
        } catch (ExtWeatherServiceError e) {
            weatherResponse.setStatus(AppConstants.ERROR);
            weatherResponse.setMessage(e.getMessage());
            return new ResponseEntity<>(weatherResponse, HttpStatus.NOT_FOUND);
        } catch (ExtWeatherServiceNotAvailable e) {
            weatherResponse.setStatus(AppConstants.ERROR);
            weatherResponse.setMessage(AppConstants.WEATHER_SERVICE_UNREACHABLE);
            return new ResponseEntity<>(weatherResponse, HttpStatus.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            weatherResponse.setStatus(AppConstants.ERROR);
            weatherResponse.setMessage(AppConstants.GENERAL_ERROR);
            return new ResponseEntity<>(weatherResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        weatherResponse.setStatus(AppConstants.SUCCESS);
        weatherResponse.setMessage(AppConstants.WEATHER_SERVICE_SUCCESS_MESSAGE);
        return new ResponseEntity<>(weatherResponse, HttpStatus.OK);
    }

}
