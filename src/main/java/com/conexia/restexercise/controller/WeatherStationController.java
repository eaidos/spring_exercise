package com.conexia.restexercise.controller;

import com.conexia.restexercise.constants.AppConstants;
import com.conexia.restexercise.domain.Coordinates;
import com.conexia.restexercise.domain.Country;
import com.conexia.restexercise.domain.WeatherStation;
import com.conexia.restexercise.dto.internal.request.WeatherStationRequestDTO;
import com.conexia.restexercise.dto.internal.response.WeatherStationResponseDTO;
import com.conexia.restexercise.exception.CountryNotFoundException;
import com.conexia.restexercise.exception.WeatherStationNotFoundException;
import com.conexia.restexercise.service.CountryService;
import com.conexia.restexercise.service.WeatherStationService;
import lombok.val;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/weatherstation")
public class WeatherStationController {

    @Autowired
    WeatherStationService weatherStationService;

    @Autowired
    CountryService countryService;

    @GetMapping(value = "/")
    ResponseEntity<WeatherStationResponseDTO> getAllWeatherStations() {

        val weatherStationResponse = new WeatherStationResponseDTO();
        List<WeatherStation> wsList = new ArrayList<WeatherStation>();
        weatherStationResponse.setData(wsList);

        try {
            wsList = this.weatherStationService.getAllWeatherStations();
            weatherStationResponse.setData(wsList);
            if (wsList.size() == 0) {
                throw new WeatherStationNotFoundException();
            }
        } catch (WeatherStationNotFoundException e) {
            weatherStationResponse.setStatus(AppConstants.ERROR);
            weatherStationResponse.setMessage(AppConstants.FIND_ALL_WEATHER_STATION_EMPTY_MESSAGE);
            return new ResponseEntity<>(weatherStationResponse, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            weatherStationResponse.setStatus(AppConstants.ERROR);
            weatherStationResponse.setMessage(AppConstants.FIND_ALL_WEATHER_STATION_ERROR_MESSAGE);
            return new ResponseEntity<>(weatherStationResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        weatherStationResponse.setStatus(AppConstants.SUCCESS);
        weatherStationResponse.setMessage(AppConstants.FIND_ALL_WEATHER_STATION_SUCCESS_MESSAGE);
        return new ResponseEntity<>(weatherStationResponse, HttpStatus.OK);
    }

    @GetMapping(value = "/{stationId:\\d+}")
    ResponseEntity<WeatherStationResponseDTO> getWeatherStationById(@PathVariable Long stationId) {

        val weatherStationResponse = new WeatherStationResponseDTO();
        val weatherStationList = new ArrayList<WeatherStation>();
        Optional<WeatherStation> optWeatherStation;
        weatherStationResponse.setData(weatherStationList);

        try {
            optWeatherStation = this.weatherStationService.getWeatherStationById(stationId);
            if (optWeatherStation.isPresent()) {
                weatherStationList.add(optWeatherStation.get());
                weatherStationResponse.setData(weatherStationList);
            } else {
                throw new WeatherStationNotFoundException();
            }
        } catch (WeatherStationNotFoundException e) {
            weatherStationResponse.setStatus(AppConstants.ERROR);
            weatherStationResponse.setMessage(AppConstants.FIND_WEATHER_STATION_EMPTY_MESSAGE);
            return new ResponseEntity<>(weatherStationResponse, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            weatherStationResponse.setStatus(AppConstants.ERROR);
            weatherStationResponse.setMessage(AppConstants.FIND_WEATHER_STATION_ERROR_MESSAGE);
            return new ResponseEntity<>(weatherStationResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        weatherStationResponse.setStatus(AppConstants.SUCCESS);
        weatherStationResponse.setMessage(AppConstants.FIND_WEATHER_STATION_SUCCESS_MESSAGE);
        return new ResponseEntity<>(weatherStationResponse, HttpStatus.OK);
    }

    @GetMapping(value = "/{stationCode:[A-Z]+}")
    ResponseEntity<WeatherStationResponseDTO> getWeatherStationByCode(@PathVariable String stationCode) {

        val weatherStationResponse = new WeatherStationResponseDTO();
        val weatherStationList = new ArrayList<WeatherStation>();
        Optional<WeatherStation> weatherStation;
        weatherStationResponse.setData(weatherStationList);

        try {
            weatherStation = this.weatherStationService.getWeatherStationByCode(stationCode);
            if (weatherStation.isPresent()) {
                weatherStationList.add(weatherStation.get());
                weatherStationResponse.setData(weatherStationList);
                weatherStationResponse.setStatus(AppConstants.SUCCESS);
            } else {
                throw new WeatherStationNotFoundException();
            }
        } catch (WeatherStationNotFoundException e) {
            weatherStationResponse.setStatus(AppConstants.ERROR);
            weatherStationResponse.setMessage(AppConstants.FIND_WEATHER_STATION_EMPTY_MESSAGE);
            return new ResponseEntity<>(weatherStationResponse, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            weatherStationResponse.setStatus(AppConstants.ERROR);
            weatherStationResponse.setMessage(AppConstants.FIND_WEATHER_STATION_ERROR_MESSAGE);
            return new ResponseEntity<>(weatherStationResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        weatherStationResponse.setMessage(AppConstants.FIND_WEATHER_STATION_SUCCESS_MESSAGE);
        return new ResponseEntity<>(weatherStationResponse, HttpStatus.OK);
    }


    @GetMapping(value = "/country/{countryId:\\d+}")
    ResponseEntity<WeatherStationResponseDTO> getAllWeatherStationsByCountryId(@PathVariable Long countryId) {

        val weatherStationResponse = new WeatherStationResponseDTO();
        List<WeatherStation> wsList = new ArrayList<WeatherStation>();

        Optional<Country> optCountry;
        weatherStationResponse.setData(wsList);

        try {
            optCountry = this.countryService.getCountryById(countryId);
            if(optCountry.isPresent()){
                wsList = this.weatherStationService.getAllStationsByCountryId(countryId);
                weatherStationResponse.setData(wsList);
                if (wsList.size() == 0) {
                    throw new WeatherStationNotFoundException();
                }
            } else {
                throw new CountryNotFoundException();
            }
        } catch (WeatherStationNotFoundException e) {
            weatherStationResponse.setStatus(AppConstants.ERROR);
            weatherStationResponse.setMessage(AppConstants.FIND_ALL_WEATHER_STATION_EMPTY_MESSAGE);
            return new ResponseEntity<>(weatherStationResponse, HttpStatus.NOT_FOUND);
        } catch (CountryNotFoundException e) {
            weatherStationResponse.setStatus(AppConstants.ERROR);
            weatherStationResponse.setMessage(AppConstants.FIND_COUNTRY_EMPTY_MESSAGE);
            return new ResponseEntity<>(weatherStationResponse, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            weatherStationResponse.setStatus(AppConstants.ERROR);
            weatherStationResponse.setMessage(AppConstants.FIND_ALL_WEATHER_STATION_ERROR_MESSAGE);
            return new ResponseEntity<>(weatherStationResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        weatherStationResponse.setStatus(AppConstants.SUCCESS);
        weatherStationResponse.setMessage(AppConstants.FIND_ALL_WEATHER_STATION_SUCCESS_MESSAGE);
        return new ResponseEntity<>(weatherStationResponse, HttpStatus.OK);
    }

    @PutMapping(value = "/{stationCode:[A-Z]+}")
    ResponseEntity<WeatherStationResponseDTO> modifyWeatherStationByCode(@PathVariable String stationCode,
                                                                         @RequestBody WeatherStationRequestDTO request)
    {
        val weatherStationResponse = new WeatherStationResponseDTO();
        val weatherStationList = new ArrayList<WeatherStation>();
        Optional<WeatherStation> optWeatherStation;
        Optional<Country> optCountry;
        var weatherStation = new WeatherStation();
        weatherStationResponse.setData(weatherStationList);

        try {
                optCountry = this.countryService.getCountryById(request.getCountryId());
                if (optCountry.isPresent()) {
                    optWeatherStation = this.weatherStationService.getWeatherStationByCode(stationCode);
                    if (optWeatherStation.isPresent()) {
                        weatherStation = modifyStation(request,optWeatherStation.get(),optCountry.get());
                        weatherStationList.add(weatherStation);
                        weatherStationResponse.setData(weatherStationList);
                        weatherStationResponse.setStatus(AppConstants.SUCCESS);
                    } else {
                        throw new WeatherStationNotFoundException();
                    }
                } else {
                    throw new CountryNotFoundException();
                }
        } catch (CountryNotFoundException e) {
            weatherStationResponse.setStatus(AppConstants.ERROR);
            weatherStationResponse.setMessage(AppConstants.FIND_COUNTRY_EMPTY_MESSAGE);
            return new ResponseEntity<>(weatherStationResponse, HttpStatus.NOT_FOUND);
        } catch (WeatherStationNotFoundException e) {
            weatherStationResponse.setStatus(AppConstants.ERROR);
            weatherStationResponse.setMessage(AppConstants.FIND_WEATHER_STATION_EMPTY_MESSAGE);
            return new ResponseEntity<>(weatherStationResponse, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            weatherStationResponse.setStatus(AppConstants.ERROR);
            weatherStationResponse.setMessage(AppConstants.UPDATE_WEATHER_STATION_ERROR_MESSAGE);
            return new ResponseEntity<>(weatherStationResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        weatherStationResponse.setMessage(AppConstants.UPDATE_WEATHER_STATION_SUCCESS_MESSAGE);
        return new ResponseEntity<>(weatherStationResponse, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{stationCode:[A-Z]+}")
    ResponseEntity<WeatherStationResponseDTO> deleteStationByCode(@PathVariable String stationCode)
    {
        val weatherStationResponse = new WeatherStationResponseDTO();
        val weatherStationList = new ArrayList<WeatherStation>();
        Optional<WeatherStation> optStation;
        var weatherStation = new WeatherStation();
        weatherStationResponse.setData(weatherStationList);

        try {
            optStation = this.weatherStationService.getWeatherStationByCode(stationCode);

            if (optStation.isPresent()) {
                weatherStation = deleteStation(optStation);
                weatherStationList.add(weatherStation);
                weatherStationResponse.setData(weatherStationList);
                weatherStationResponse.setStatus(AppConstants.SUCCESS);
            } else {
                throw new WeatherStationNotFoundException();
            }
        } catch (WeatherStationNotFoundException e) {
            weatherStationResponse.setStatus(AppConstants.ERROR);
            weatherStationResponse.setMessage(AppConstants.FIND_WEATHER_STATION_EMPTY_MESSAGE);
            return new ResponseEntity<>(weatherStationResponse, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            weatherStationResponse.setStatus(AppConstants.ERROR);
            weatherStationResponse.setMessage(AppConstants.DELETE_WEATHER_STATION_ERROR_MESSAGE);
            return new ResponseEntity<>(weatherStationResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        weatherStationResponse.setMessage(AppConstants.DELETE_WEATHER_STATION_SUCCESS_MESSAGE);
        return new ResponseEntity<>(weatherStationResponse, HttpStatus.OK);
    }

    @PostMapping(value = "/")
    ResponseEntity<WeatherStationResponseDTO> createWeatherStation(@RequestBody WeatherStationRequestDTO request) {

        val wsResponse = new WeatherStationResponseDTO();
        val wsList = new ArrayList<WeatherStation>();
        WeatherStation ws;
        wsResponse.setData(wsList);
        Optional<Country> optCountry;
        wsResponse.setData(wsList);

        try {
            optCountry = this.countryService.getCountryById(request.getCountryId());
            if (optCountry.isPresent()){
                ws = createNewWeatherStation(request,optCountry.get());
                wsList.add(ws);
                wsResponse.setStatus(AppConstants.SUCCESS);
            } else {
                throw new CountryNotFoundException();
            }
        } catch (CountryNotFoundException e) {
            wsResponse.setStatus(AppConstants.ERROR);
            wsResponse.setMessage(AppConstants.FIND_COUNTRY_EMPTY_MESSAGE);
            return new ResponseEntity<>(wsResponse, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            wsResponse.setStatus(AppConstants.ERROR);
            wsResponse.setMessage(AppConstants.CREATE_WEATHER_STATION_ERROR_MESSAGE);
            return new ResponseEntity<>(wsResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        wsResponse.setMessage(AppConstants.CREATE_WEATHER_STATION_SUCCESS_MESSAGE);
        return new ResponseEntity<>(wsResponse, HttpStatus.OK);

    }

    private WeatherStation modifyStation(WeatherStationRequestDTO request, WeatherStation ws,
                                         Country country) {
        ws.setDescription(request.getDescription());
        ws.setCode(request.getCode());
        ws.setCoordinates(request.getCoordinates());
        ws.setDateUpdated(LocalDate.now());
        ws.setCountry(country);
        this.weatherStationService.updateWeatherStation(ws);
        return ws;
    }

    private WeatherStation deleteStation(Optional<WeatherStation> optStation) {
        val station = optStation.get();
        this.weatherStationService.deleteWeatherStation(station);
        return station;
    }

    private WeatherStation createNewWeatherStation(WeatherStationRequestDTO request, Country country) {
        var ws = new WeatherStation();
        ws.setDescription(request.getDescription());
        ws.setCoordinates(new Coordinates(request.getCoordinates().getLatitude(),
                                          request.getCoordinates().getLongitude()));
        ws.setCode(request.getCode());
        ws.setCountry(country);
        ws.setDateCreated(LocalDate.now());
        ws.setDeleted(false);
        return this.weatherStationService.createWeatherStation(ws);
    }
}
