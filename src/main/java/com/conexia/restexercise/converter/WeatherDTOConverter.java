package com.conexia.restexercise.converter;

import com.conexia.restexercise.dto.external.response.DailyWeatherResponseDTO;
import com.conexia.restexercise.dto.internal.response.WeatherResponseDTO;
import lombok.var;

public class WeatherDTOConverter {

    public static WeatherResponseDTO fromExternalToInternalDTOResponse(DailyWeatherResponseDTO external,
                                                                       WeatherResponseDTO internal)
    {
        internal.setData(external.getWeatherResponse());
        return internal;
    }
}
