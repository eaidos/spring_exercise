package com.conexia.restexercise.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Where(clause="deleted=0")
@Entity
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter private long id;

    @Column(name = "CITY_NAME")
    @Getter @Setter private String cityName;

    @Embedded
    @Getter @Setter private Coordinates coordinates;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "COUNTRY_ID", nullable = false,referencedColumnName = "ID")
    @Getter @Setter private Country country;

    @Column(name = "DELETED")
    @Getter @Setter private boolean deleted;

    @Column(name = "DATE_CREATED")
    @Getter @Setter private LocalDate dateCreated;

    @Column(name = "DATE_DELETED")
    @Getter @Setter private LocalDate dateDeleted;

    @Column(name = "DATE_UPDATED")
    @Getter @Setter private LocalDate dateUpdated;
}
