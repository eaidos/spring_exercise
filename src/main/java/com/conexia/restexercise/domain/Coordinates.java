package com.conexia.restexercise.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class Coordinates {

    @Column(name = "latitude")
    @Getter @Setter private String latitude;

    @Column(name = "longitude")
    @Getter @Setter private String longitude;
}
