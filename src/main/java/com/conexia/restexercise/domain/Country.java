package com.conexia.restexercise.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Where(clause="deleted=0")
@Entity
@Table(
        uniqueConstraints = @UniqueConstraint(columnNames = {"COUNTRY_CODE"})
)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter private long id;

    @Column(nullable = false, name="COUNTRY_CODE")
    @Getter @Setter private String code;

    @Column(nullable = false, name="DESCRIPTION")
    @Getter @Setter private String description;

    @Column(name = "DELETED")
    @Getter @Setter private boolean deleted;

    @Column(name = "DATE_CREATED")
    @Getter @Setter private LocalDate dateCreated;

    @Column(name = "DATE_DELETED")
    @Getter @Setter private LocalDate dateDeleted;

    @Column(name = "DATE_UPDATED")
    @Getter @Setter private LocalDate dateUpdated;

}
