package com.conexia.restexercise.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter private long id;

    @Column(name = "USER")
    @Getter @Setter private long user;

    @Column(name = "REQUEST")
    @Getter @Setter private String request;

    @Column(name = "METHOD")
    @Getter @Setter private String method;

    @Column(name = "DATE_RECEIVED")
    @Getter @Setter private LocalDate dateReceived;
    
}
