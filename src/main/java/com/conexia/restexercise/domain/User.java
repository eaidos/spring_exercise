package com.conexia.restexercise.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter private long id;

    @Column(name = "user_id")
    @Getter @Setter private String userId;

    @Column(name = "DATE_CREATED")
    @Getter @Setter private LocalDate dateCreated;

    @Column(name = "DELETED")
    @Getter @Setter private boolean deleted;
}
