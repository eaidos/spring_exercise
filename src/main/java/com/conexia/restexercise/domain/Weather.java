package com.conexia.restexercise.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Weather {
    @JsonProperty("wind_cdir")
    @Getter @Setter private String abbreviatedWindDirection;

    @JsonProperty("rh")
    @Getter @Setter private int relativeHumidity;

    @JsonProperty("pod")
    @Getter @Setter private String dayOrNight;

    @JsonProperty("lon")
    @Getter @Setter private String longitude;

    @JsonProperty("pres")
    @Getter @Setter private BigDecimal pressure;

    @JsonProperty("timezone")
    @Getter @Setter private String timezone;

    @JsonProperty("ob_time")
    @Getter @Setter private String observationTime;

    @JsonProperty("country_code")
    @Getter @Setter private String countryCode;

    @JsonProperty("clouds")
    @Getter @Setter private BigDecimal cloudCoveragePercentage;

    @JsonProperty("vis")
    @Getter @Setter private BigDecimal visibility;

    @JsonProperty("wind_spd")
    @Getter @Setter private BigDecimal windSpeed;

    @JsonProperty("wind_cdir_full")
    @Getter @Setter private String windDirectionDescription;

    @JsonProperty("app_temp")
    @Getter @Setter private BigDecimal feelsLikeTemperature;

    @JsonProperty("state_code")
    @Getter @Setter private String stateCode;

    @JsonProperty("ts")
    @Getter @Setter private long lastObservationOfDay;

    @JsonProperty("h_angle")
    @Getter @Setter private BigDecimal solarHourAngle;

    @JsonProperty("dewpt")
    @Getter @Setter private BigDecimal dewPoint;

    @JsonProperty("weather")
    @Getter @Setter private WeatherIcon weatherIcon;

    @JsonProperty("uv")
    @Getter @Setter private int uv;

    @JsonProperty("aqi")
    @Getter @Setter private int airQualityIndex;

    @JsonProperty("station")
    @Getter @Setter private String station;

    @JsonProperty("wind_dir")
    @Getter @Setter private BigDecimal windDirection;

    @JsonProperty("elev_angle")
    @Getter @Setter private BigDecimal solarElevationAngle;

    @JsonProperty("datetime")
    @Getter @Setter private String currentCycleHour;

    @JsonProperty("precip")
    @Getter @Setter private BigDecimal precipitationRate;

    @JsonProperty("ghi")
    @Getter @Setter private BigDecimal globalHorizontalSolarIrradiance;

    @JsonProperty("dni")
    @Getter @Setter private BigDecimal directNormalSolarIrradiance;

    @JsonProperty("dhi")
    @Getter @Setter private BigDecimal diffuseHorizontalSolarIrradiance;

    @JsonProperty("solar_rad")
    @Getter @Setter private BigDecimal estimatedSolarRadiation;

    @JsonProperty("city_name")
    @Getter @Setter private String cityName;

    @JsonProperty("sunset")
    @Getter @Setter private String sunsetTime;

    @JsonProperty("sunrise")
    @Getter @Setter private String sunriseTime;

    @JsonProperty("temp")
    @Getter @Setter private BigDecimal temperature;

    @JsonProperty("lat")
    @Getter @Setter private String latitude;

    @JsonProperty("slp")
    @Getter @Setter private BigDecimal seaLevelPressure;

    @JsonProperty("snow")
    @Getter @Setter private BigDecimal snowfallAmount;
}
