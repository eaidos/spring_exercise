package com.conexia.restexercise.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherIcon {

    @JsonProperty("icon")
    @Getter @Setter private String icon;

    @JsonProperty("code")
    @Getter @Setter private String code;

    @JsonProperty("description")
    @Getter @Setter private String description;

}
