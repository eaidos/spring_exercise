package com.conexia.restexercise.dto.external.request;

import com.conexia.restexercise.domain.Coordinates;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class WeatherByCoordinatesRequestDTO {
    @Getter @Setter private Coordinates coordinates;

    public WeatherByCoordinatesRequestDTO(final String latitude, final String longitude) {
        this.coordinates = new Coordinates();
        this.coordinates.setLatitude(latitude);
        this.coordinates.setLongitude(longitude);
    }

}

