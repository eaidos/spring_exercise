package com.conexia.restexercise.dto.external.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class WeatherByPostalRequestDTO {

    @Getter @Setter private String postalCode;

}
