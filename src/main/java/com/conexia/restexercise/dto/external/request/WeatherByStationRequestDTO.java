package com.conexia.restexercise.dto.external.request;

import com.conexia.restexercise.domain.Coordinates;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class WeatherByStationRequestDTO {
    @Getter @Setter private String code;

}
