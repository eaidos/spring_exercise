package com.conexia.restexercise.dto.external.response;

import com.conexia.restexercise.domain.Weather;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DailyWeatherResponseDTO {

    @JsonProperty("count")
    @Getter @Setter long countOfObservations;

    @JsonProperty("data")
    @Getter @Setter List<Weather> weatherResponse;

    @JsonProperty("error")
    @Getter @Setter String error;

}
