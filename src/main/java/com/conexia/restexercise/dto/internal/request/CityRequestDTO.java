package com.conexia.restexercise.dto.internal.request;

import com.conexia.restexercise.domain.Coordinates;
import lombok.Getter;
import lombok.Setter;


public class CityRequestDTO {
    @Getter @Setter private String cityName;
    @Getter @Setter private Coordinates coordinates;
    @Getter @Setter private long countryId;
}
