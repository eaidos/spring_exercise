package com.conexia.restexercise.dto.internal.request;

import lombok.Getter;
import lombok.Setter;

public class CountryRequestDTO {
    @Getter @Setter private String description;
    @Getter @Setter private String code;
}
