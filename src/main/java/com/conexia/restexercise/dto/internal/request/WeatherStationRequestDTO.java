package com.conexia.restexercise.dto.internal.request;

import com.conexia.restexercise.domain.Coordinates;
import com.conexia.restexercise.domain.Country;
import lombok.Getter;
import lombok.Setter;

public class WeatherStationRequestDTO {

    @Getter @Setter private Coordinates coordinates;
    @Getter @Setter private String description;
    @Getter @Setter private String code;
    @Getter @Setter private Long countryId;

}
