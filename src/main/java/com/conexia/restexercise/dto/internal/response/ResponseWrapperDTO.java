package com.conexia.restexercise.dto.internal.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

public abstract class  ResponseWrapperDTO {

    @Setter @Getter private String status;
    @Setter @Getter private String message;
    @Setter @Getter private List<?> data;

}
