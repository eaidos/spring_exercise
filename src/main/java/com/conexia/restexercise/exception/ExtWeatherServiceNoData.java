package com.conexia.restexercise.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class ExtWeatherServiceNoData extends RuntimeException {

    @Getter @Setter String errorMessage;
    @Getter @Setter int statusCode;
}
