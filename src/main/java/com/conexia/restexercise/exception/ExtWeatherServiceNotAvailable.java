package com.conexia.restexercise.exception;

import lombok.Getter;
import lombok.Setter;

public class ExtWeatherServiceNotAvailable extends RuntimeException {

    @Getter @Setter String errorMessage;

    public ExtWeatherServiceNotAvailable(String errorMessage){
        this.errorMessage = errorMessage;
    }
}
