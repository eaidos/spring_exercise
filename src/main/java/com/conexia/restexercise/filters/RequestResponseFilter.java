package com.conexia.restexercise.filters;

import com.conexia.restexercise.domain.Request;
import com.conexia.restexercise.service.RequestService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.LocalDate;

@Component
public class RequestResponseFilter implements Filter {

    @Autowired
    RequestService requestService;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        val myRequest = new Request();
        HttpServletRequest req = (HttpServletRequest) request;

        myRequest.setUser(1);
        myRequest.setDateReceived(LocalDate.now());
        myRequest.setRequest(req.getRequestURI());
        myRequest.setMethod(req.getMethod());

        requestService.saveRequest(myRequest);
        chain.doFilter(request, response);

    }
}