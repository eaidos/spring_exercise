package com.conexia.restexercise.repository;

import com.conexia.restexercise.domain.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ICountryRepository extends JpaRepository<Country, Long>, QueryByExampleExecutor<Country> {


}