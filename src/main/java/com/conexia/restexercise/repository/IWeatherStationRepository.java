package com.conexia.restexercise.repository;

import com.conexia.restexercise.domain.WeatherStation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IWeatherStationRepository extends JpaRepository<WeatherStation, Long> {

}
