package com.conexia.restexercise.service;

import com.conexia.restexercise.domain.City;
import com.conexia.restexercise.domain.Country;
import com.conexia.restexercise.repository.ICityRepository;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class CityService {

    @Autowired
    private ICityRepository cityRepository;

    @Transactional(readOnly=true)
    public List<City> getAllCities() {
        return cityRepository.findAll();
    }

    @Transactional(readOnly=true)
    public Optional<City> getCityById(long cityId) {
        return cityRepository.findById(cityId);
    }

    @Transactional(readOnly=true)
    public List<City> getAllCitiesByCountryId(long countryId) {
        val city = new City();
        val country = new Country();

        country.setId(countryId);
        country.setDeleted(false);
        city.setCountry(country);
        city.setDeleted(false);

        ExampleMatcher em = ExampleMatcher.matchingAll()
                                          .withIgnorePaths("id","cityName","coordinates","dateCreated",
                                                           "dateDeleted","dateUpdated");
        Example<City> cityExample = Example.of(city, em);
        return cityRepository.findAll(cityExample);
    }

    @Transactional(readOnly=true)
    public List<City> getAllCitiesByCountryCode(String countryCode,long countryId) {
        val city = new City();
        val country = new Country();

        country.setCode(countryCode);
        country.setDeleted(false);
        country.setId(countryId);
        city.setCountry(country);
        city.setDeleted(false);

        ExampleMatcher em = ExampleMatcher.matchingAll()
                                          .withIgnorePaths("id","cityName","coordinates","dateCreated",
                                                           "dateDeleted","dateUpdated");
        Example<City> cityExample = Example.of(city, em);
        return cityRepository.findAll(cityExample);
    }

    @Transactional
    public City createCity(City city) {
        return cityRepository.save(city);
    }

    @Transactional
    public City updateCity(City city) {
        return cityRepository.save(city);
    }

}

