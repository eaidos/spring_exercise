package com.conexia.restexercise.service;

import com.conexia.restexercise.domain.Country;
import com.conexia.restexercise.repository.ICountryRepository;
import lombok.NonNull;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class CountryService {

    @Autowired
    private ICountryRepository countryRepository;

    @Transactional(readOnly=true)
    public List<Country> getAllCountries() {
        return countryRepository.findAll();
    }

    @Transactional
    public Country updateCountry(Country country) {
        return countryRepository.save(country);
    }

    @Transactional
    public Country createCountry(Country country) {
        return countryRepository.save(country);
    }

    @Transactional(readOnly=true)
    public Optional<Country> getCountryById(@NonNull long countryId) {
        return countryRepository.findById(countryId);
    }

    @Transactional(readOnly=true)
    public Optional<Country> getCountryByCode(@NonNull String countryCode, boolean includeDeleted) {
        val country = new Country();
        country.setCode(countryCode);
        if (!includeDeleted) {
            country.setDeleted(false);
        }

        ExampleMatcher em = ExampleMatcher.matchingAll()
                                          .withIgnorePaths("id","description","dateCreated","dateDeleted","dateUpdated");
        Example<Country> countryExample = Example.of(country,em);
        return countryRepository.findOne(countryExample);
    }

}