package com.conexia.restexercise.service;

import com.conexia.restexercise.domain.Request;
import com.conexia.restexercise.repository.IRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RequestService {

    @Autowired
    private IRequestRepository requestRepository;

    @Transactional
    public Request saveRequest(Request request) {
        return requestRepository.save(request);
    }
}
