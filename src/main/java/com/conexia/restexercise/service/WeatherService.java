package com.conexia.restexercise.service;

import com.conexia.restexercise.constants.AppConstants;
import com.conexia.restexercise.dto.external.request.WeatherByCoordinatesRequestDTO;
import com.conexia.restexercise.dto.external.request.WeatherByPostalRequestDTO;
import com.conexia.restexercise.dto.external.request.WeatherByStationRequestDTO;
import com.conexia.restexercise.dto.external.response.DailyWeatherResponseDTO;
import com.conexia.restexercise.exception.ExtWeatherServiceError;
import com.conexia.restexercise.exception.ExtWeatherServiceNoData;
import com.conexia.restexercise.exception.ExtWeatherServiceNotAvailable;
import lombok.val;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class WeatherService {

    @Value("${weather.api_endpoint}")
    private String url;

    @Value("${weather.api_key}")
    private String api_key;



    public DailyWeatherResponseDTO getCurrentWeather( UriComponentsBuilder uriBuilder ){
        val headers = new HttpHeaders();
        val restTemplate = new RestTemplate();
        DailyWeatherResponseDTO dwResponse = new DailyWeatherResponseDTO();

        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<?> entity = new HttpEntity<>(headers);

        try {
            ResponseEntity<DailyWeatherResponseDTO> response = restTemplate.exchange(
                    uriBuilder.toUriString(),
                    HttpMethod.GET,
                    entity,
                    DailyWeatherResponseDTO.class);

            if (response.getStatusCodeValue() == HttpStatus.NO_CONTENT.value()) {
                throw new ExtWeatherServiceNoData(AppConstants.WEATHER_SERVICE_NO_DATA ,response.getStatusCodeValue());
            } else {
                dwResponse.setCountOfObservations(response.getBody().getCountOfObservations());
                dwResponse.setWeatherResponse(response.getBody().getWeatherResponse());
                dwResponse.setError(response.getBody().getError());
            }
        } catch (ExtWeatherServiceNoData e) {
            throw e;
        } catch (HttpClientErrorException e) {
            throw new ExtWeatherServiceError(e.getResponseBodyAsString(),e.getRawStatusCode());
        } catch (Exception e){
            throw new ExtWeatherServiceNotAvailable(e.getMessage());
        }

        return dwResponse;
    }

    public DailyWeatherResponseDTO getCurrentWeatherByCoordinates(WeatherByCoordinatesRequestDTO req) {

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(this.url + AppConstants.WEATHER_SERVICE_CURRENT_PATH)
                                                           .queryParam("lat", req.getCoordinates().getLatitude())
                                                           .queryParam("lon", req.getCoordinates().getLongitude())
                                                           .queryParam("key", this.api_key);
        return getCurrentWeather(uriBuilder);
    }


    public DailyWeatherResponseDTO getCurrentWeatherByStation(WeatherByStationRequestDTO req) {

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(this.url + AppConstants.WEATHER_SERVICE_CURRENT_PATH)
                                                              .queryParam("station", req.getCode())
                                                              .queryParam("key", this.api_key);

        return getCurrentWeather(uriBuilder);
    }

    public DailyWeatherResponseDTO getCurrentWeatherByPostalCode(WeatherByPostalRequestDTO req) {

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(this.url + AppConstants.WEATHER_SERVICE_CURRENT_PATH)
                                                              .queryParam("postal_code", req.getPostalCode())
                                                              .queryParam("key", this.api_key);

        return getCurrentWeather(uriBuilder);
    }

}
