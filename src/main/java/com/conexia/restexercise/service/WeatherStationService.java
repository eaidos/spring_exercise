package com.conexia.restexercise.service;

import com.conexia.restexercise.domain.Country;
import com.conexia.restexercise.domain.WeatherStation;
import com.conexia.restexercise.repository.IWeatherStationRepository;
import lombok.NonNull;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class WeatherStationService {

    @Autowired
    private IWeatherStationRepository weatherStationRepository;

    @Transactional
    public WeatherStation updateWeatherStation(@NonNull WeatherStation weatherStation) {
        return weatherStationRepository.save(weatherStation);
    }

    @Transactional
    public WeatherStation createWeatherStation(WeatherStation ws) {
        return weatherStationRepository.save(ws);
    }

    @Transactional
    public void deleteWeatherStation(WeatherStation weatherStation) {
        weatherStationRepository.delete(weatherStation);
    }

    @Transactional(readOnly=true)
    public List<WeatherStation> getAllWeatherStations() {
        return weatherStationRepository.findAll();
    }

    @Transactional(readOnly=true)
    public Optional<WeatherStation> getWeatherStationById(@NonNull long stationId) {
        return weatherStationRepository.findById(stationId);
    }

    @Transactional(readOnly=true)
    public Optional<WeatherStation> getWeatherStationByCode(String stationCode) {
        val weatherStation = new WeatherStation();
        weatherStation.setCode(stationCode);
        weatherStation.setDeleted(false);

        ExampleMatcher em = ExampleMatcher.matchingAll()
                                          .withIgnorePaths("id","coordinates","description","country",
                                                           "dateCreated","dateDeleted","dateUpdated");
        Example<WeatherStation> wsExample = Example.of(weatherStation,em);
        return weatherStationRepository.findOne(wsExample);
    }

    @Transactional(readOnly=true)
    public List<WeatherStation> getAllStationsByCountryId(long countryId) {
        val weatherStation = new WeatherStation();
        val country = new Country();
        country.setId(countryId);
        country.setDeleted(false);
        weatherStation.setCountry(country);
        weatherStation.setDeleted(false);

        ExampleMatcher em = ExampleMatcher.matchingAll()
                                          .withIgnorePaths("id","description","coordinates",
                                                           "dateCreated","dateDeleted","dateUpdated");
        Example<WeatherStation> weatherStationExample = Example.of(weatherStation, em);
        return weatherStationRepository.findAll(weatherStationExample);
    }

    @Transactional(readOnly=true)
    public List<WeatherStation> getAllStationsByCountryCode(String countryCode) {
        val weatherStation = new WeatherStation();
        val country = new Country();
        country.setCode(countryCode);
        country.setDeleted(false);
        weatherStation.setCountry(country);
        weatherStation.setDeleted(false);

        ExampleMatcher em = ExampleMatcher.matchingAll()
                                          .withIgnorePaths("id","description","coordinates",
                                                           "dateCreated","dateDeleted","dateUpdated");
        Example<WeatherStation> weatherStationExample = Example.of(weatherStation, em);
        return weatherStationRepository.findAll(weatherStationExample);
    }

}
