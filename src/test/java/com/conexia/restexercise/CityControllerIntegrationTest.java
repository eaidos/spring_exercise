package com.conexia.restexercise;

import com.conexia.restexercise.constants.AppConstants;
import com.conexia.restexercise.domain.Coordinates;
import com.conexia.restexercise.dto.internal.request.CityRequestDTO;
import com.conexia.restexercise.dto.internal.response.CityResponseDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedHashMap;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
public class CityControllerIntegrationTest {

    private final String BASE_URI = "http://localhost:8080/restexercise/city/";
    private final int MAX_ID = Integer.MAX_VALUE;
    private RestTemplate template;

    @Before
    public void init(){
        template = new RestTemplate();
    }

    //=============================================================================================================//

    @Test
    public void testGetAllCities() {
        ResponseEntity<CityResponseDTO> response = template.getForEntity(BASE_URI,CityResponseDTO.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getData().isEmpty(),is(false));
    }

    //=============================================================================================================//

    @Test
    public void testGetCityById() {
        ResponseEntity<CityResponseDTO> response = template.getForEntity(BASE_URI+"1",CityResponseDTO.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getData().isEmpty(),is(false));
        val city = (LinkedHashMap<?,?>) response.getBody().getData().get(0);
        assertEquals(city.get("id"),1);
    }

    @Test
    public void testGetCityByIdNotFound() throws JsonProcessingException {
        try {
            ResponseEntity<CityResponseDTO> response = template.getForEntity(BASE_URI + MAX_ID, CityResponseDTO.class);
        } catch (HttpClientErrorException e) {
            assertThat(e.getStatusCode(), is(HttpStatus.NOT_FOUND));
            ObjectMapper om = new ObjectMapper();
            CityResponseDTO responseDTO = om.readValue(e.getResponseBodyAsString(), CityResponseDTO.class);
            assertThat(responseDTO.getData().isEmpty(),is(true));
            assertEquals(responseDTO.getStatus(),AppConstants.ERROR);
        }
    }

    //=============================================================================================================//

    @Test
    public void testGetAllCitiesByCountryId() {
        ResponseEntity<CityResponseDTO> response = template.getForEntity(BASE_URI+"country/1",CityResponseDTO.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getData().isEmpty(),is(false));
    }

    @Test
    public void testGetAllCitiesByCountryIdNotFound() throws JsonProcessingException {
        try {
            ResponseEntity<CityResponseDTO> response = template.getForEntity(BASE_URI + "country/" + MAX_ID,
                                                                             CityResponseDTO.class);
        } catch (HttpClientErrorException e) {
            assertThat(e.getStatusCode(), is(HttpStatus.NOT_FOUND));
            ObjectMapper om = new ObjectMapper();
            CityResponseDTO responseDTO = om.readValue(e.getResponseBodyAsString(), CityResponseDTO.class);
            assertThat(responseDTO.getData().isEmpty(),is(true));
            assertEquals(responseDTO.getStatus(),AppConstants.ERROR);
        }
    }

    //=============================================================================================================//

    @Test
    public void testGetAllCitiesByCountryCode() {
        ResponseEntity<CityResponseDTO> response = template.getForEntity(BASE_URI+"country/AR",CityResponseDTO.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getData().isEmpty(),is(false));
    }

    @Test
    public void testGetAllCitiesByCountryCodeNotFound() throws JsonProcessingException {
        try {
            ResponseEntity<CityResponseDTO> response = template.getForEntity(BASE_URI+"country/ZZZZ",
                                                                             CityResponseDTO.class);
        } catch (HttpClientErrorException e) {
            assertThat(e.getStatusCode(), is(HttpStatus.NOT_FOUND));
            ObjectMapper om = new ObjectMapper();
            CityResponseDTO responseDTO = om.readValue(e.getResponseBodyAsString(), CityResponseDTO.class);
            assertThat(responseDTO.getData().isEmpty(),is(true));
            assertEquals(responseDTO.getStatus(),AppConstants.ERROR);
        }
    }

    //=============================================================================================================//

    @Test
    public void testModifyCityById() {
        val cityRequestDTO = new CityRequestDTO();
        val coords = new Coordinates();
        cityRequestDTO.setCityName("Barranquilla");
        cityRequestDTO.setCountryId(1);
        coords.setLongitude("1");
        coords.setLatitude("-2");
        cityRequestDTO.setCoordinates(coords);
        template.put(BASE_URI+"2",cityRequestDTO);
    }

    @Test
    public void testModifyCityByIdNotFound() throws JsonProcessingException {

        val cityRequestDTO = new CityRequestDTO();
        val coords = new Coordinates();
        cityRequestDTO.setCityName("Barranquilla");
        cityRequestDTO.setCountryId(1);
        coords.setLongitude("1");
        coords.setLatitude("-2");
        cityRequestDTO.setCoordinates(coords);
        try {
            template.put(BASE_URI+MAX_ID,cityRequestDTO);
        } catch (HttpClientErrorException e) {
            assertThat(e.getStatusCode(), is(HttpStatus.NOT_FOUND));
            ObjectMapper om = new ObjectMapper();
            CityResponseDTO responseDTO = om.readValue(e.getResponseBodyAsString(), CityResponseDTO.class);
            assertThat(responseDTO.getData().isEmpty(),is(true));
            assertEquals(responseDTO.getStatus(),AppConstants.ERROR);
        }
    }
    //=============================================================================================================//

    @Test
    public void testCreate() {
        val cityRequestDTO = new CityRequestDTO();
        val coords = new Coordinates();
        cityRequestDTO.setCityName("Barranquilla");
        cityRequestDTO.setCountryId(1);
        coords.setLongitude("1");
        coords.setLatitude("-2");
        cityRequestDTO.setCoordinates(coords);
        ResponseEntity<CityResponseDTO> response = template.postForEntity(BASE_URI, cityRequestDTO, CityResponseDTO.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getData().isEmpty(),is(false));
    }

    @Test
    public void testCreateCityCountryNotFound() throws JsonProcessingException {

        val cityRequestDTO = new CityRequestDTO();
        val coords = new Coordinates();
        cityRequestDTO.setCityName("Barranquilla");
        cityRequestDTO.setCountryId(MAX_ID);
        coords.setLongitude("1");
        coords.setLatitude("-2");
        cityRequestDTO.setCoordinates(coords);
        try {
            ResponseEntity<CityResponseDTO> response = template.postForEntity(BASE_URI, cityRequestDTO, CityResponseDTO.class);
        } catch (HttpClientErrorException e) {
            assertThat(e.getStatusCode(), is(HttpStatus.NOT_FOUND));
            ObjectMapper om = new ObjectMapper();
            CityResponseDTO responseDTO = om.readValue(e.getResponseBodyAsString(), CityResponseDTO.class);
            assertThat(responseDTO.getData().isEmpty(),is(true));
            assertEquals(responseDTO.getStatus(),AppConstants.ERROR);
        }
    }

    //=============================================================================================================//

    @Test
    public void testdeleteCityById() {
            template.delete(BASE_URI+"1");
    }

    @Test
    public void testdeleteCityByIdFail() throws JsonProcessingException {
        try {
            template.delete(BASE_URI+MAX_ID);
        } catch (HttpClientErrorException e) {
            assertThat(e.getStatusCode(), is(HttpStatus.NOT_FOUND));
            ObjectMapper om = new ObjectMapper();
            CityResponseDTO responseDTO = om.readValue(e.getResponseBodyAsString(), CityResponseDTO.class);
            assertThat(responseDTO.getData().isEmpty(),is(true));
            assertEquals(responseDTO.getStatus(),AppConstants.ERROR);
        }
    }

}
