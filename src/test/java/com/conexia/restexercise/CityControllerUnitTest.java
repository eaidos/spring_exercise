package com.conexia.restexercise;

import com.conexia.restexercise.constants.AppConstants;
import com.conexia.restexercise.controller.CityController;
import com.conexia.restexercise.domain.City;
import com.conexia.restexercise.domain.Coordinates;
import com.conexia.restexercise.domain.Country;
import com.conexia.restexercise.dto.internal.request.CityRequestDTO;
import com.conexia.restexercise.dto.internal.response.CityResponseDTO;
import com.conexia.restexercise.service.CityService;
import com.conexia.restexercise.service.CountryService;
import com.conexia.restexercise.service.RequestService;
import com.conexia.restexercise.service.WeatherService;
import com.conexia.restexercise.service.WeatherStationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest
public class CityControllerUnitTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RequestService requestService;

    @MockBean
    private CityService cityService;

    @MockBean
    private WeatherStationService weatherStationService;

    @MockBean
    private WeatherService weatherService;

    @MockBean
    private CountryService countryService;

    @InjectMocks
    private CityController cityController;
    
    private final int MAX_ID = Integer.MAX_VALUE;
    private final String MAX_CODE = "ZZZZ";

    private List<City> cityListWithOneUpdated;
    private List<City> cityListWithThreeOneDeleted;
    private List<City> cityListWithThreeUpdated;
    private List<City> cityListWithOneDeleted;
    private List<City> cityListWithThree;
    private List<City> cityListWithTwo;
    private List<City> cityListWithOne;
    private List<City> cityListWithOneCreated;
    private List<City> emptyCityList;
    private City city;
    private City updatedCity;
    private City createdCity;
    private City deletedCity;
    private Country countryA;
    private Country countryC;
    private ObjectMapper om;



    @Before
    public void init(){
        countryA = new Country(1, "AR", "Argentina",
                                      false, LocalDate.now(),null,null);

        countryC = new Country(2, "CO", "Colombia",
                                      false, LocalDate.now(),null,null);

        city = new City(1,"City A", new Coordinates("1","1"),
                        countryA,false, LocalDate.now(), null, null);

        createdCity = new City(0,"City A", new Coordinates("1","1"),
                        countryA,false, LocalDate.now(), null, null);

        updatedCity = new City(1,"City A Updated", new Coordinates("99","99"),
                               countryC,false, LocalDate.now(), null, LocalDate.now());

        deletedCity = new City(1,"City A", new Coordinates("1","1"),
                               countryA,true, LocalDate.now(), LocalDate.now(), null);

        cityListWithOneUpdated = Arrays.asList(updatedCity);

        cityListWithOneDeleted = Arrays.asList(
                new City(1,"City A", new Coordinates("1","1"),
                         countryA,true, LocalDate.now(), LocalDate.now(), null));

        cityListWithOneCreated = Arrays.asList(createdCity);

        cityListWithTwo = Arrays.asList(
                new City(1,"City A", new Coordinates("1","1"),
                         countryA,false, LocalDate.now(), null, null),
                new City(2,"City B", new Coordinates("2","2"),
                         countryA,false, LocalDate.now(), null, null)
                );

        cityListWithThree = Arrays.asList(
                new City(1,"City A", new Coordinates("1","1"),
                         countryA,false, LocalDate.now(), null, null),
                new City(2,"City B", new Coordinates("2","2"),
                         countryA,false, LocalDate.now(), null, null),
                new City(3,"City C", new Coordinates("3","3"),
                         countryC,false, LocalDate.now(), null, null)
                );

        cityListWithThreeOneDeleted = Arrays.asList(
                new City(1,"City A", new Coordinates("1","1"),
                         countryA,true, LocalDate.now(), LocalDate.now(), null),
                new City(2,"City B", new Coordinates("2","2"),
                         countryA,false, LocalDate.now(), null, null),
                new City(3,"City C", new Coordinates("3","3"),
                         countryC,false, LocalDate.now(), null, null)
        );

        cityListWithThreeUpdated = Arrays.asList(
                new City(1,"City A Updated", new Coordinates("99","99"),
                         countryC,false, LocalDate.now(), null, LocalDate.now()),
                new City(2,"City B", new Coordinates("2","2"),
                         countryA,false, LocalDate.now(), null, null),
                new City(3,"City C", new Coordinates("3","3"),
                         countryC,false, LocalDate.now(), null, null)
                );

        cityListWithOne = Arrays.asList(
                new City(1,"City A", new Coordinates("1","1"),
                         countryA,false, LocalDate.now(), null, null));

        emptyCityList = Arrays.asList();

        om = new ObjectMapper();
        om.registerModule(new JavaTimeModule());
        om.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    //=============================================================================================================//

    @Test
    public void testGetAllCities() throws Exception {

        CityResponseDTO cityResponseDTO = new CityResponseDTO();
        cityResponseDTO.setStatus(AppConstants.SUCCESS);
        cityResponseDTO.setMessage(AppConstants.FIND_ALL_CITIES_SUCCESS_MESSAGE);
        cityResponseDTO.setData(cityListWithTwo);

        when(cityService.getAllCities()).thenReturn(cityListWithTwo);

        mockMvc.perform(get("/city/"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(cityResponseDTO)));

        verify(cityService, times(1)).getAllCities();
        verifyNoMoreInteractions(cityService);

    }

    @Test
    public void testGetAllCitiesFail() throws Exception {

        CityResponseDTO cityResponseDTO = new CityResponseDTO();
        cityResponseDTO.setStatus(AppConstants.ERROR);
        cityResponseDTO.setMessage(AppConstants.FIND_ALL_CITIES_EMPTY_MESSAGE);
        cityResponseDTO.setData(emptyCityList);

        when(cityService.getAllCities()).thenReturn(emptyCityList);

        mockMvc.perform(get("/city/"))
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(cityResponseDTO)));

        verify(cityService, times(1)).getAllCities();
        verifyNoMoreInteractions(cityService);

    }

    //=============================================================================================================//

    @Test
    public void testGetCityById() throws Exception {

        CityResponseDTO cityResponseDTO = new CityResponseDTO();
        cityResponseDTO.setStatus(AppConstants.SUCCESS);
        cityResponseDTO.setMessage(AppConstants.FIND_CITY_SUCCESS_MESSAGE);
        cityResponseDTO.setData(cityListWithOne);

        when(cityService.getCityById(1)).thenReturn(java.util.Optional.ofNullable(city));

        mockMvc.perform(get("/city/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(cityResponseDTO)));

        verify(cityService, times(1)).getCityById(1);
        verifyNoMoreInteractions(cityService);
    }

    @Test
    public void testGetCityByIdFail() throws Exception {
        CityResponseDTO cityResponseDTO = new CityResponseDTO();
        cityResponseDTO.setStatus(AppConstants.ERROR);
        cityResponseDTO.setMessage(AppConstants.FIND_CITY_EMPTY_MESSAGE);
        cityResponseDTO.setData(emptyCityList);

        when(cityService.getCityById(MAX_ID)).thenReturn(java.util.Optional.ofNullable(null));

        mockMvc.perform(get("/city/"+MAX_ID))
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(cityResponseDTO)));

        verify(cityService, times(1)).getCityById(MAX_ID);
        verifyNoMoreInteractions(cityService);
    }

    //=============================================================================================================//

    @Test
    public void testGetAllCitiesByCountryId() throws Exception {
        CityResponseDTO cityResponseDTO = new CityResponseDTO();
        cityResponseDTO.setStatus(AppConstants.SUCCESS);
        cityResponseDTO.setMessage(AppConstants.FIND_ALL_CITIES_SUCCESS_MESSAGE);
        cityResponseDTO.setData(cityListWithTwo);

        when(cityService.getAllCitiesByCountryId(1)).thenReturn(cityListWithTwo);
        when(countryService.getCountryById(1)).thenReturn(java.util.Optional.ofNullable(countryA));

        mockMvc.perform(get("/city/country/"+1))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(cityResponseDTO)));

        verify(cityService, times(1)).getAllCitiesByCountryId(1);
        verify(countryService, times(1)).getCountryById(1);
        verifyNoMoreInteractions(cityService);
    }

    @Test
    public void testGetAllCitiesByCountryIdFail() throws Exception {
        CityResponseDTO cityResponseDTO = new CityResponseDTO();
        cityResponseDTO.setStatus(AppConstants.ERROR);
        cityResponseDTO.setMessage(AppConstants.FIND_COUNTRY_EMPTY_MESSAGE);
        cityResponseDTO.setData(emptyCityList);

        when(cityService.getAllCitiesByCountryId(MAX_ID)).thenReturn(emptyCityList);

        mockMvc.perform(get("/city/country/"+MAX_ID))
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(cityResponseDTO)));

        verify(countryService, times(1)).getCountryById(MAX_ID);
        verifyNoMoreInteractions(cityService);
    }

    //=============================================================================================================//

    @Test
    public void testGetAllCitiesByCountryCode() throws Exception {
        CityResponseDTO cityResponseDTO = new CityResponseDTO();
        cityResponseDTO.setStatus(AppConstants.SUCCESS);
        cityResponseDTO.setMessage(AppConstants.FIND_ALL_CITIES_SUCCESS_MESSAGE);
        cityResponseDTO.setData(cityListWithTwo);

        when(cityService.getAllCitiesByCountryCode("AR", 1)).thenReturn(cityListWithTwo);
        when(countryService.getCountryByCode("AR",AppConstants.NOT_INCLUDE_DELETED))
                .thenReturn(java.util.Optional.ofNullable(countryA));


        mockMvc.perform(get("/city/country/AR"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(cityResponseDTO)));

        verify(cityService, times(1)).getAllCitiesByCountryCode("AR",1);
        verify(countryService, times(1)).getCountryByCode("AR",
                                                                                AppConstants.NOT_INCLUDE_DELETED);

        verifyNoMoreInteractions(cityService);
    }

    @Test
    public void testGetAllCitiesByCountryCodeFail() throws Exception {
        CityResponseDTO cityResponseDTO = new CityResponseDTO();
        cityResponseDTO.setStatus(AppConstants.ERROR);
        cityResponseDTO.setMessage(AppConstants.FIND_COUNTRY_EMPTY_MESSAGE);
        cityResponseDTO.setData(emptyCityList);

        when(cityService.getAllCitiesByCountryCode(MAX_CODE,MAX_ID)).thenReturn(emptyCityList);

        mockMvc.perform(get("/city/country/"+MAX_ID))
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(cityResponseDTO)));

        verifyNoMoreInteractions(cityService);
    }

    //=============================================================================================================//

    @Test
    public void createCity() throws Exception {
        CityResponseDTO cityResponseDTO = new CityResponseDTO();
        cityResponseDTO.setStatus(AppConstants.SUCCESS);
        cityResponseDTO.setMessage(AppConstants.CREATE_CITY_SUCCESS_MESSAGE);
        cityResponseDTO.setData(cityListWithOneCreated);

        CityRequestDTO request = new CityRequestDTO();
        request.setCoordinates(new Coordinates("1","1"));
        request.setCountryId(1);
        request.setCityName("City A");

        when(cityService.createCity(any())).thenReturn(createdCity);
        when(countryService.getCountryById(request.getCountryId())).thenReturn(java.util.Optional.ofNullable(countryA));

        mockMvc.perform(post("/city/")
                                .content(om.writeValueAsString(request))
                                .contentType(MediaType.APPLICATION_JSON)
        )
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(cityResponseDTO)));

        verify(cityService, times(1)).createCity(any());
        verify(countryService, times(1)).getCountryById(request.getCountryId());
        verifyNoMoreInteractions(cityService);
    }

    @Test
    public void createCityFail() throws Exception {
        CityResponseDTO cityResponseDTO = new CityResponseDTO();
        cityResponseDTO.setStatus(AppConstants.ERROR);
        cityResponseDTO.setMessage(AppConstants.FIND_COUNTRY_EMPTY_MESSAGE);
        cityResponseDTO.setData(emptyCityList);

        CityRequestDTO request = new CityRequestDTO();
        request.setCoordinates(new Coordinates("1","1"));
        request.setCountryId(MAX_ID);
        request.setCityName("City A");

        when(countryService.getCountryById(request.getCountryId())).thenReturn(java.util.Optional.ofNullable(null));

        mockMvc.perform(post("/city/")
                                .content(om.writeValueAsString(request))
                                .contentType(MediaType.APPLICATION_JSON)
        )
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(cityResponseDTO)));

        verify(countryService, times(1)).getCountryById(request.getCountryId());
        verifyNoMoreInteractions(cityService);
    }

    //=============================================================================================================//

    @Test
    public void testModifyCityById() throws Exception {
        CityResponseDTO cityResponseDTO = new CityResponseDTO();
        cityResponseDTO.setStatus(AppConstants.SUCCESS);
        cityResponseDTO.setMessage(AppConstants.UPDATE_CITY_SUCCESS_MESSAGE);
        cityResponseDTO.setData(cityListWithOneUpdated);

        CityRequestDTO request = new CityRequestDTO();
        request.setCoordinates(new Coordinates("99","99"));
        request.setCountryId(2);
        request.setCityName("City A Updated");

        when(cityService.updateCity(city)).thenReturn(updatedCity);
        when(cityService.getCityById(1)).thenReturn(java.util.Optional.ofNullable(city));
        when(countryService.getCountryById(request.getCountryId())).thenReturn(java.util.Optional.ofNullable(countryC));

        mockMvc.perform(put("/city/1")
                                .content(om.writeValueAsString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                       )
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(cityResponseDTO)));

        verify(cityService, times(1)).updateCity(city);
        verify(cityService, times(1)).getCityById(1);
        verify(countryService, times(1)).getCountryById(request.getCountryId());
        verifyNoMoreInteractions(cityService);
    }

    @Test
    public void testModifyCityByIdFail() throws Exception {
        CityResponseDTO cityResponseDTO = new CityResponseDTO();
        cityResponseDTO.setStatus(AppConstants.ERROR);
        cityResponseDTO.setMessage(AppConstants.FIND_CITY_EMPTY_MESSAGE);
        cityResponseDTO.setData(emptyCityList);

        CityRequestDTO request = new CityRequestDTO();
        request.setCoordinates(new Coordinates("99","99"));
        request.setCountryId(1);
        request.setCityName("City A Updated");

        when(countryService.getCountryById(request.getCountryId())).thenReturn(java.util.Optional.ofNullable(countryA));
        when(cityService.getCityById(request.getCountryId())).thenReturn(java.util.Optional.ofNullable(null));

        mockMvc.perform(put("/city/"+MAX_ID)
                                .content(om.writeValueAsString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                       )
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(cityResponseDTO)));

        verify(cityService, times(1)).getCityById(MAX_ID);
        verify(countryService, times(1)).getCountryById(request.getCountryId());
        verifyNoMoreInteractions(cityService);
    }

    //=============================================================================================================//

    @Test
    public void testDeleteCityById() throws Exception {
        CityResponseDTO cityResponseDTO = new CityResponseDTO();
        cityResponseDTO.setStatus(AppConstants.SUCCESS);
        cityResponseDTO.setMessage(AppConstants.DELETE_CITY_SUCCESS_MESSAGE);
        cityResponseDTO.setData(cityListWithOneDeleted);

        when(cityService.updateCity(city)).thenReturn(deletedCity);
        when(cityService.getCityById(1)).thenReturn(java.util.Optional.ofNullable(city));

        mockMvc.perform(delete("/city/1") )
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(cityResponseDTO)));

        verify(cityService, times(1)).updateCity(city);
        verify(cityService, times(1)).getCityById(1);

    }

    @Test
    public void testDeleteCityByIdFail() throws Exception {
        CityResponseDTO cityResponseDTO = new CityResponseDTO();
        cityResponseDTO.setStatus(AppConstants.ERROR);
        cityResponseDTO.setMessage(AppConstants.DELETE_CITY_EMPTY_MESSAGE);
        cityResponseDTO.setData(emptyCityList);

        when(cityService.updateCity(city)).thenReturn(deletedCity);

        mockMvc.perform(get("/city/"+MAX_ID))
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(cityResponseDTO)));

        verify(cityService, times(1)).getCityById(MAX_ID);
        verifyNoMoreInteractions(cityService);

    }
}