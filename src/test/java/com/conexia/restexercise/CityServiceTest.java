package com.conexia.restexercise;

import com.conexia.restexercise.domain.City;
import com.conexia.restexercise.domain.Coordinates;
import com.conexia.restexercise.domain.Country;
import com.conexia.restexercise.repository.ICityRepository;
import com.conexia.restexercise.service.CityService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;


import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CityServiceTest {

    @Mock
    ICityRepository cityRepository;

    @InjectMocks
    private CityService cityService;

    private final long MAX_ID = Long.MAX_VALUE;
    private final String MAX_CODE = "ZZZZ";

    private List<City> cityListWithTwo;
    private List<City> emptyCityList;
    private Optional<City> city;
    private City createdCity;
    private Country countryA;

    @Before
    public void init(){
        countryA = new Country(1, "AR", "Argentina",
                               false, LocalDate.now(), null, null);

        city = Optional.of(new City(1, "City A", new Coordinates("1", "1"),
                                    countryA, false, LocalDate.now(), null, null));

        createdCity = new City(0,"City A", new Coordinates("1","1"),
                               countryA,false, LocalDate.now(), null, null);

        cityListWithTwo = Arrays.asList(
                new City(1,"City A", new Coordinates("1","1"),
                         countryA,false, LocalDate.now(), null, null),
                new City(2,"City B", new Coordinates("2","2"),
                         countryA,false, LocalDate.now(), null, null)
        );

        emptyCityList = Arrays.asList();

    }


    @Test
    public void testGetAllCities() {
        List<City> cityList;
        when(cityRepository.findAll()).thenReturn(cityListWithTwo);

        cityList = cityService.getAllCities();

        assertEquals(cityList.size(),2);
        assertEquals(cityList.get(0).getId(), 1);
        verify(cityRepository, times(1)).findAll();
        verifyNoMoreInteractions(cityRepository);
    }

    //=============================================================================================================//

    @Test
    public void testGetCityById() {
        Optional<City> result;
        when(cityRepository.findById(1L)).thenReturn(city);

        result = cityService.getCityById(1);

        assertEquals(city, result);
        verify(cityRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(cityRepository);
    }

    @Test
    public void testGetCityByIdFail() {
        Optional<City> result;
        when(cityRepository.findById(MAX_ID)).thenReturn(null);

        result = cityService.getCityById(MAX_ID);

        assertNull(result);
        verify(cityRepository, times(1)).findById(MAX_ID);
        verifyNoMoreInteractions(cityRepository);
    }

    //=============================================================================================================//

    @Test
    public void testGetAllCitiesByCountryId() {
        List<City> cityList;
        ExampleMatcher em = ExampleMatcher.matchingAny();
        Example<City> cityExample = Example.of(new City(), em);
        when(cityRepository.findAll( any( cityExample.getClass())) ).thenReturn(cityListWithTwo);

        cityList = cityService.getAllCitiesByCountryId(1);

        assertEquals(cityList.size(),2);
        assertEquals(cityList.get(0).getId(), 1);
        verify(cityRepository, times(1)).findAll(any(cityExample.getClass()));
        verifyNoMoreInteractions(cityRepository);

    }

    @Test
    public void testGetAllCitiesByCountryIdFail() {
        List<City> cityList;
        ExampleMatcher em = ExampleMatcher.matchingAny();
        Example<City> cityExample = Example.of(new City(), em);
        when(cityRepository.findAll( any( cityExample.getClass())) ).thenReturn(emptyCityList);

        cityList = cityService.getAllCitiesByCountryId(MAX_ID);

        assertEquals(cityList.size(),0);
        verify(cityRepository, times(1)).findAll(any(cityExample.getClass()));
        verifyNoMoreInteractions(cityRepository);

    }

    //=============================================================================================================//

    @Test
    public void testGetAllCitiesByCountryCode() {
        List<City> cityList;
        ExampleMatcher em = ExampleMatcher.matchingAny();
        Example<City> cityExample = Example.of(new City(), em);
        when(cityRepository.findAll( any( cityExample.getClass())) ).thenReturn(cityListWithTwo);

        cityList = cityService.getAllCitiesByCountryCode("AR", 1);

        assertEquals(cityList.size(),2);
        assertEquals(cityList.get(0).getId(), 1);
        assertEquals(cityList.get(1).getId(), 2);
        assertEquals(cityList.get(0).getCountry().getCode(), "AR");
        assertEquals(cityList.get(1).getCountry().getCode(), "AR");
        verify(cityRepository, times(1)).findAll(any(cityExample.getClass()));
        verifyNoMoreInteractions(cityRepository);

    }

    @Test
    public void testGetAllCitiesByCountryCodeFail() {
        List<City> cityList;
        ExampleMatcher em = ExampleMatcher.matchingAny();
        Example<City> cityExample = Example.of(new City(), em);
        when(cityRepository.findAll( any( cityExample.getClass())) ).thenReturn(emptyCityList);

        cityList = cityService.getAllCitiesByCountryCode(MAX_CODE, MAX_ID);

        assertEquals(cityList.size(),0);
        verify(cityRepository, times(1)).findAll(any(cityExample.getClass()));
        verifyNoMoreInteractions(cityRepository);
    }

    //=============================================================================================================//

    @Test
    public void testCreateCity() {

        City c;
        when(cityRepository.save(any(City.class))).thenReturn(createdCity);

        c = cityService.createCity(createdCity);

        assertEquals(c.getId(), createdCity.getId());
        verify(cityRepository, times(1)).save(createdCity);
        verifyNoMoreInteractions(cityRepository);

    }

    @Test
    public void testCreateCityFail() {

        City c;
        when(cityRepository.save(null)).thenThrow(IllegalArgumentException.class);

        assertThrows(IllegalArgumentException.class, () -> cityService.createCity(null));

        verify(cityRepository, times(1)).save(null);
        verifyNoMoreInteractions(cityRepository);

    }

    //=============================================================================================================//


    @Test
    public void testUpdateCity() {

        City c;
        when(cityRepository.save(any(City.class))).thenReturn(createdCity);

        c = cityService.createCity(createdCity);

        assertEquals(c.getId(), createdCity.getId());
        verify(cityRepository, times(1)).save(createdCity);
        verifyNoMoreInteractions(cityRepository);

    }

    @Test
    public void testUpdateCityFail() {

        when(cityRepository.save(null)).thenThrow(IllegalArgumentException.class);

        assertThrows(IllegalArgumentException.class, () -> cityService.createCity(null));

        verify(cityRepository, times(1)).save(null);
        verifyNoMoreInteractions(cityRepository);

    }

    //=============================================================================================================//


}