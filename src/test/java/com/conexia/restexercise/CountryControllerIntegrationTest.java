package com.conexia.restexercise;

import com.conexia.restexercise.constants.AppConstants;
import com.conexia.restexercise.dto.internal.request.CountryRequestDTO;
import com.conexia.restexercise.dto.internal.response.CountryResponseDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedHashMap;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
public class CountryControllerIntegrationTest {

    private final String BASE_URI = "http://localhost:8080/restexercise/country/";
    private final long MAX_ID = Long.MAX_VALUE;
    private final String MAX_CODE = "ZZZZ";
    private RestTemplate template;

    @Before
    public void init(){
        template = new RestTemplate();
    }

    //=============================================================================================================//

    @Test
    public void testGetAllCountries() {
        ResponseEntity<CountryResponseDTO> response = template.getForEntity(BASE_URI, CountryResponseDTO.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getData().isEmpty(),is(false));
    }

    //=============================================================================================================//

    @Test
    public void testGetCountryById() {
        ResponseEntity<CountryResponseDTO> response = template.getForEntity(BASE_URI+"AR",CountryResponseDTO.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getData().isEmpty(),is(false));
        val country = (LinkedHashMap<?,?>) response.getBody().getData().get(0);
        assertEquals(country.get("id"),2);
    }

    @Test
    public void testGetCountryByIdNotFound() throws JsonProcessingException {
        try {
            ResponseEntity<CountryResponseDTO> response = template.getForEntity(BASE_URI + MAX_CODE,
                                                                                CountryResponseDTO.class);
        } catch (HttpClientErrorException e) {
            assertThat(e.getStatusCode(), is(HttpStatus.NOT_FOUND));
            ObjectMapper om = new ObjectMapper();
            CountryResponseDTO responseDTO = om.readValue(e.getResponseBodyAsString(), CountryResponseDTO.class);
            assertThat(responseDTO.getData().isEmpty(),is(true));
            assertEquals(responseDTO.getStatus(),AppConstants.ERROR);
        }
    }

    //=============================================================================================================//

    @Test
    public void testGetCountryByCode() {
        ResponseEntity<CountryResponseDTO> response = template.getForEntity(BASE_URI+"AR",CountryResponseDTO.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getData().isEmpty(),is(false));
    }

    @Test
    public void testGeCountryByCodeNotFound() throws JsonProcessingException {
        try {
            ResponseEntity<CountryResponseDTO> response = template.getForEntity(BASE_URI+MAX_CODE,
                                                                             CountryResponseDTO.class);
        } catch (HttpClientErrorException e) {
            assertThat(e.getStatusCode(), is(HttpStatus.NOT_FOUND));
            ObjectMapper om = new ObjectMapper();
            CountryResponseDTO responseDTO = om.readValue(e.getResponseBodyAsString(), CountryResponseDTO.class);
            assertThat(responseDTO.getData().isEmpty(),is(true));
            assertEquals(responseDTO.getStatus(),AppConstants.ERROR);
        }
    }

    //=============================================================================================================//

    @Test
    public void testModifyCountryByCode() {
        val countryRequestDTO = new CountryRequestDTO();
        countryRequestDTO.setDescription("Switzerland");
        countryRequestDTO.setCode("CH");
        template.put(BASE_URI+"AR",countryRequestDTO);
    }

    @Test
    public void testModifyCountryByCodeNotFound() throws JsonProcessingException {
        val countryRequestDTO = new CountryRequestDTO();
        countryRequestDTO.setDescription("Switzerland");
        countryRequestDTO.setCode("CH");
        try {
            template.put(BASE_URI+MAX_CODE,countryRequestDTO);
        } catch (HttpClientErrorException e) {
            assertThat(e.getStatusCode(), is(HttpStatus.NOT_FOUND));
            ObjectMapper om = new ObjectMapper();
            CountryResponseDTO responseDTO = om.readValue(e.getResponseBodyAsString(), CountryResponseDTO.class);
            assertThat(responseDTO.getData().isEmpty(),is(true));
            assertEquals(responseDTO.getStatus(),AppConstants.ERROR);
        }
    }
    //=============================================================================================================//

    @Test
    public void testCreateCountry() {
        val countryRequestDTO = new CountryRequestDTO();
        countryRequestDTO.setDescription("Switzerland");
        countryRequestDTO.setCode("CH");
        ResponseEntity<CountryResponseDTO> response = template.postForEntity(BASE_URI, countryRequestDTO,
                                                                             CountryResponseDTO.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getData().isEmpty(),is(false));
    }

    @Test
    public void testCreateCountryFail() throws JsonProcessingException {

        val countryRequestDTO = new CountryRequestDTO();
        countryRequestDTO.setDescription("Switzerland");
        countryRequestDTO.setCode(null);
        try {
            ResponseEntity<CountryResponseDTO> response = template.postForEntity(BASE_URI, countryRequestDTO,
                                                                                 CountryResponseDTO.class);
        } catch (HttpServerErrorException e) {
            assertThat(e.getStatusCode(), is(HttpStatus.INTERNAL_SERVER_ERROR));
            ObjectMapper om = new ObjectMapper();
            CountryResponseDTO responseDTO = om.readValue(e.getResponseBodyAsString(), CountryResponseDTO.class);
            assertThat(responseDTO.getData().isEmpty(),is(true));
            assertEquals(responseDTO.getStatus(),AppConstants.ERROR);
        }
    }

    //=============================================================================================================//

    @Test
    public void testdeleteCountryByCode() {
        template.delete(BASE_URI+"CN");
    }

    @Test
    public void testdeleteCountryByCodeNotFound() throws JsonProcessingException {
        try {
            template.delete(BASE_URI+MAX_CODE);
        } catch (HttpClientErrorException e) {
            assertThat(e.getStatusCode(), is(HttpStatus.NOT_FOUND));
            ObjectMapper om = new ObjectMapper();
            CountryResponseDTO responseDTO = om.readValue(e.getResponseBodyAsString(), CountryResponseDTO.class);
            assertThat(responseDTO.getData().isEmpty(),is(true));
            assertEquals(responseDTO.getStatus(),AppConstants.ERROR);
        }
    }

    @Test
    public void testdeleteCountryByCodeFail() throws JsonProcessingException {
        try {
            template.delete(BASE_URI+"AR");
        } catch (HttpClientErrorException e) {
            assertThat(e.getStatusCode(), is(HttpStatus.FORBIDDEN));
            ObjectMapper om = new ObjectMapper();
            CountryResponseDTO responseDTO = om.readValue(e.getResponseBodyAsString(), CountryResponseDTO.class);
            assertThat(responseDTO.getData().isEmpty(),is(true));
            assertEquals(responseDTO.getStatus(),AppConstants.ERROR);
        }
    }

}