package com.conexia.restexercise;

import com.conexia.restexercise.constants.AppConstants;
import com.conexia.restexercise.controller.CityController;
import com.conexia.restexercise.domain.Country;
import com.conexia.restexercise.dto.internal.request.CountryRequestDTO;
import com.conexia.restexercise.dto.internal.response.CountryResponseDTO;
import com.conexia.restexercise.service.CityService;
import com.conexia.restexercise.service.CountryService;
import com.conexia.restexercise.service.RequestService;
import com.conexia.restexercise.service.WeatherService;
import com.conexia.restexercise.service.WeatherStationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest
public class CountryControllerUnitTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RequestService requestService;

    @MockBean
    private CityService cityService;

    @MockBean
    private WeatherStationService weatherStationService;

    @MockBean
    private WeatherService weatherService;

    @MockBean
    private CountryService countryService;

    @InjectMocks
    private CityController cityController;

    private final long MAX_ID = Long.MAX_VALUE;
    private final String MAX_CODE = "ZZZZ";
    private final String COUNTRY_PATH = "/country/";


    private List<Country> countryListWithOneDeleted;
    private List<Country> countryListWithOneUpdated;
    private List<Country> countryListWithTwo;
    private List<Country> countryListWithOne;
    private List<Country> emptyCountryList;
    private Optional<Country> countryAOpt;
    private Country countryA;
    private Country countryC;
    private Country countryCH;
    private ObjectMapper om;
    private Country countryAUpdated;



    @Before
    public void init(){
        countryA = new Country(1, "AR", "Argentina",
                               false, LocalDate.now(),null,null);

        countryC = new Country(2, "CO", "Colombia",
                               false, LocalDate.now(),null,null);

        countryCH = new Country(3, "CH", "China",
                               false, LocalDate.now(),null,null);

        countryAUpdated = new Country(1, "CH", "China",
                                      false, LocalDate.now(),null,LocalDate.now());

        countryListWithTwo = Arrays.asList(
                new Country(1,  "CH", "China",
                            false, LocalDate.now(),null,LocalDate.now()),
                new Country(2, "CO", "Colombia",
                            false, LocalDate.now(),null,null)
        );

        countryListWithOneUpdated = Arrays.asList(
                new Country(1, "CH", "China",
                            false, LocalDate.now(),null,LocalDate.now())
        );

        countryListWithOneDeleted = Arrays.asList(
                new Country(1, "AR", "Argentina",
                            true, LocalDate.now(),LocalDate.now(),null)
        );

        countryListWithOne = Arrays.asList(
                new Country(1, "AR", "Argentina",
                            false, LocalDate.now(),null,null)
        );

        countryAOpt = Optional.of(new Country(1, "AR", "Argentina",
                                              false, LocalDate.now(), null, null));

        emptyCountryList = Arrays.asList();

        om = new ObjectMapper();
        om.registerModule(new JavaTimeModule());
        om.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    //=============================================================================================================//

    @Test
    public void testGetAllCountries() throws Exception {

        CountryResponseDTO countryResponseDTO = new CountryResponseDTO();
        countryResponseDTO.setStatus(AppConstants.SUCCESS);
        countryResponseDTO.setMessage(AppConstants.FIND_ALL_COUNTRIES_SUCCESS_MESSAGE);
        countryResponseDTO.setData(countryListWithTwo);

        when(countryService.getAllCountries()).thenReturn(countryListWithTwo);

        mockMvc.perform(get(COUNTRY_PATH))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(countryResponseDTO)));

        verify(countryService, times(1)).getAllCountries();
        verifyNoMoreInteractions(countryService);

    }

    @Test
    public void testGetAllCountriesFail() throws Exception {

        CountryResponseDTO countryResponseDTO = new CountryResponseDTO();
        countryResponseDTO.setStatus(AppConstants.ERROR);
        countryResponseDTO.setMessage(AppConstants.FIND_ALL_COUNTRIES_EMPTY_MESSAGE);
        countryResponseDTO.setData(emptyCountryList);

        when(countryService.getAllCountries()).thenReturn(emptyCountryList);

        mockMvc.perform(get(COUNTRY_PATH))
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(countryResponseDTO)));

        verify(countryService, times(1)).getAllCountries();
        verifyNoMoreInteractions(countryService);

    }

    //=============================================================================================================//

    @Test
    public void testGetCountryById() throws Exception {

        CountryResponseDTO countryResponseDTO = new CountryResponseDTO();
        countryResponseDTO.setStatus(AppConstants.SUCCESS);
        countryResponseDTO.setMessage(AppConstants.FIND_COUNTRY_SUCCESS_MESSAGE);
        countryResponseDTO.setData(countryListWithOne);

        when(countryService.getCountryById(1)).thenReturn(java.util.Optional.ofNullable(countryA));

        mockMvc.perform(get(COUNTRY_PATH+"1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(countryResponseDTO)));

        verify(countryService, times(1)).getCountryById(1);
        verifyNoMoreInteractions(countryService);
    }

    @Test
    public void testGetCountryByIdFail() throws Exception {

        CountryResponseDTO countryResponseDTO = new CountryResponseDTO();
        countryResponseDTO.setStatus(AppConstants.ERROR);
        countryResponseDTO.setMessage(AppConstants.FIND_COUNTRY_EMPTY_MESSAGE);
        countryResponseDTO.setData(emptyCountryList);

        when(countryService.getCountryById(MAX_ID)).thenReturn(java.util.Optional.ofNullable(null));

        mockMvc.perform(get(COUNTRY_PATH+MAX_ID))
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(countryResponseDTO)));

        verify(countryService, times(1)).getCountryById(MAX_ID);
        verifyNoMoreInteractions(countryService);
    }

    //=============================================================================================================//

    @Test
    public void testGetCountryByCode() throws Exception {
        CountryResponseDTO countryResponseDTO = new CountryResponseDTO();
        countryResponseDTO.setStatus(AppConstants.SUCCESS);
        countryResponseDTO.setMessage(AppConstants.FIND_COUNTRY_SUCCESS_MESSAGE);
        countryResponseDTO.setData(countryListWithOne);

        when(countryService.getCountryByCode("AR",AppConstants.NOT_INCLUDE_DELETED)).thenReturn(countryAOpt);

        mockMvc.perform(get(COUNTRY_PATH+"AR"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(countryResponseDTO)));

        verify(countryService, times(1)).getCountryByCode("AR",
                                                                                 AppConstants.NOT_INCLUDE_DELETED);
        verifyNoMoreInteractions(countryService);
    }

    @Test
    public void testGetCountryByCodeFail() throws Exception {
        CountryResponseDTO countryResponseDTO = new CountryResponseDTO();
        countryResponseDTO.setStatus(AppConstants.ERROR);
        countryResponseDTO.setMessage(AppConstants.FIND_COUNTRY_EMPTY_MESSAGE);
        countryResponseDTO.setData(emptyCountryList);

        when(countryService.getCountryByCode(MAX_CODE,AppConstants.NOT_INCLUDE_DELETED))
                .thenReturn(Optional.ofNullable(null));

        mockMvc.perform(get(COUNTRY_PATH+MAX_CODE))
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(countryResponseDTO)));

        verify(countryService, times(1)).getCountryByCode(MAX_CODE,AppConstants.NOT_INCLUDE_DELETED);
        verifyNoMoreInteractions(countryService);
    }

    //=============================================================================================================//
    @Test
    public void createCountry() throws Exception {
        CountryResponseDTO countryResponseDTO = new CountryResponseDTO();
        countryResponseDTO.setStatus(AppConstants.SUCCESS);
        countryResponseDTO.setMessage(AppConstants.CREATE_COUNTRY_SUCCESS_MESSAGE);
        countryResponseDTO.setData(countryListWithOne);

        CountryRequestDTO request = new CountryRequestDTO();
        request.setDescription(countryA.getDescription());
        request.setCode(countryA.getCode());

        when(countryService.createCountry(any())).thenReturn(countryA);

        mockMvc.perform(post(COUNTRY_PATH)
                                .content(om.writeValueAsString(request))
                                .contentType(MediaType.APPLICATION_JSON)
        )
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(countryResponseDTO)));

        verify(countryService, times(1)).createCountry(any());
        verify(countryService, times(1)).getCountryByCode(request.getCode(),
                                                          AppConstants.NOT_INCLUDE_DELETED);
        verifyNoMoreInteractions(countryService);
    }

    @Test
    public void createCountryFail() throws Exception {
        CountryResponseDTO countryResponseDTO = new CountryResponseDTO();
        countryResponseDTO.setStatus(AppConstants.ERROR);
        countryResponseDTO.setMessage(AppConstants.CREATE_COUNTRY_ERROR_MESSAGE);
        countryResponseDTO.setData(emptyCountryList);

        CountryRequestDTO request = new CountryRequestDTO();
        request.setDescription(null);
        request.setCode(null);

        when(countryService.createCountry(any())).thenThrow(IllegalArgumentException.class);

        mockMvc.perform(post(COUNTRY_PATH)
                                .content(om.writeValueAsString(request))
                                .contentType(MediaType.APPLICATION_JSON)
        )
               .andExpect(status().is5xxServerError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(countryResponseDTO)));

        verify(countryService, times(1)).createCountry(any());
        verify(countryService, times(1)).getCountryByCode(request.getCode(),
                                                          AppConstants.NOT_INCLUDE_DELETED);
        verifyNoMoreInteractions(countryService);
    }

    //=============================================================================================================//

    @Test
    public void testModifyCountryByCode() throws Exception {
        CountryResponseDTO countryResponseDTO = new CountryResponseDTO();
        countryResponseDTO.setStatus(AppConstants.SUCCESS);
        countryResponseDTO.setMessage(AppConstants.UPDATE_COUNTRY_SUCCESS_MESSAGE);
        countryResponseDTO.setData(countryListWithOneUpdated);

        CountryRequestDTO request = new CountryRequestDTO();
        request.setDescription(countryCH.getDescription());
        request.setCode(countryCH.getCode());

        when(countryService.getCountryByCode("AR",AppConstants.NOT_INCLUDE_DELETED))
                .thenReturn(java.util.Optional.ofNullable(countryA));
        when(countryService.updateCountry(countryA)).thenReturn(countryAUpdated);

        mockMvc.perform(put(COUNTRY_PATH+"AR")
                                .content(om.writeValueAsString(request))
                                .contentType(MediaType.APPLICATION_JSON)
        )
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(countryResponseDTO)));

        verify(countryService, times(1)).getCountryByCode("AR",AppConstants.NOT_INCLUDE_DELETED);
        verify(countryService, times(1)).updateCountry(countryA);
        verifyNoMoreInteractions(countryService);
    }

    @Test
    public void testModifyCountryByCodeFail() throws Exception {
        CountryResponseDTO countryResponseDTO = new CountryResponseDTO();
        countryResponseDTO.setStatus(AppConstants.ERROR);
        countryResponseDTO.setMessage(AppConstants.FIND_COUNTRY_EMPTY_MESSAGE);
        countryResponseDTO.setData(emptyCountryList);

        CountryRequestDTO request = new CountryRequestDTO();
        request.setDescription(countryCH.getDescription());
        request.setCode(countryCH.getCode());

        when(countryService.getCountryByCode(MAX_CODE,AppConstants.NOT_INCLUDE_DELETED))
                .thenReturn(java.util.Optional.ofNullable(null));

        mockMvc.perform(put(COUNTRY_PATH+MAX_CODE)
                                .content(om.writeValueAsString(request))
                                .contentType(MediaType.APPLICATION_JSON)
        )
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(countryResponseDTO)));

        verify(countryService, times(1)).getCountryByCode(MAX_CODE,AppConstants.NOT_INCLUDE_DELETED);
        verify(countryService, times(0)).updateCountry(any());
        verifyNoMoreInteractions(countryService);
    }

    //=============================================================================================================//

    @Test
    public void testDeleteCountryByCode() throws Exception {
        CountryResponseDTO countryResponseDTO = new CountryResponseDTO();
        countryResponseDTO.setStatus(AppConstants.SUCCESS);
        countryResponseDTO.setMessage(AppConstants.DELETE_COUNTRY_SUCCESS_MESSAGE);
        countryResponseDTO.setData(countryListWithOneDeleted);

        when(countryService.getCountryByCode("AR",AppConstants.NOT_INCLUDE_DELETED))
                .thenReturn(java.util.Optional.ofNullable(countryA));
        when(countryService.updateCountry(countryA)).thenReturn(countryAUpdated);

        mockMvc.perform(delete(COUNTRY_PATH+"AR") )
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(countryResponseDTO)));

        verify(countryService, times(1)).getCountryByCode("AR",AppConstants.NOT_INCLUDE_DELETED);
        verify(countryService, times(1)).updateCountry(any());
        verifyNoMoreInteractions(countryService);

    }

    @Test
    public void testDeleteCountryByCodeFail() throws Exception {
        CountryResponseDTO countryResponseDTO = new CountryResponseDTO();
        countryResponseDTO.setStatus(AppConstants.ERROR);
        countryResponseDTO.setMessage(AppConstants.FIND_COUNTRY_EMPTY_MESSAGE);
        countryResponseDTO.setData(emptyCountryList);

        when(countryService.getCountryByCode(MAX_CODE, AppConstants.NOT_INCLUDE_DELETED))
                           .thenReturn(java.util.Optional.ofNullable(null));

        mockMvc.perform(get(COUNTRY_PATH+MAX_CODE))
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(countryResponseDTO)));

        verify(countryService, times(1)).getCountryByCode(MAX_CODE, AppConstants.NOT_INCLUDE_DELETED);
        verify(countryService, times(0)).updateCountry(any());
        verifyNoMoreInteractions(countryService);

    }
}