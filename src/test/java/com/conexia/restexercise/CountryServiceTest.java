package com.conexia.restexercise;

import com.conexia.restexercise.constants.AppConstants;
import com.conexia.restexercise.domain.Country;
import com.conexia.restexercise.repository.ICountryRepository;
import com.conexia.restexercise.service.CountryService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;


import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CountryServiceTest {

    @Mock
    ICountryRepository countryRepository;

    @InjectMocks
    private CountryService countryService;

    private final long MAX_ID = Long.MAX_VALUE;
    private final String MAX_CODE = "ZZZZ";

    private List<Country> countryListWithTwo;
    private List<Country> emptyCountryList;
    private Optional<Country> countryAOpt;
    private Country countryA;

    @Before
    public void init(){
        countryA = new Country(1, "AR", "Argentina",
                               false, LocalDate.now(), null, null);

        countryAOpt = Optional.of(new Country(1, "AR", "Argentina",
                                              false, LocalDate.now(), null, null));

        countryListWithTwo = Arrays.asList(
                new Country(1, "AR", "Argentina",
                            false, LocalDate.now(), null, null),
                new Country(2, "CO", "Colombia",
                            false, LocalDate.now(),null,null)
        );

        emptyCountryList = Arrays.asList();

    }


    @Test
    public void testGetAllCountries() {
        List<Country> countryList;
        when(countryRepository.findAll()).thenReturn(countryListWithTwo);

        countryList = countryService.getAllCountries();

        assertEquals(countryList.size(),2);
        assertEquals(countryList.get(0).getId(), 1);
        verify(countryRepository, times(1)).findAll();
        verifyNoMoreInteractions(countryRepository);
    }

    //=============================================================================================================//

    @Test
    public void testGetCountryById() {
        Optional<Country> result;
        when(countryRepository.findById(1L)).thenReturn(countryAOpt);

        result = countryService.getCountryById(1);

        assertEquals(countryAOpt, result);
        verify(countryRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(countryRepository);
    }

    @Test
    public void testGetCountryByIdFail() {
        Optional<Country> result;
        when(countryRepository.findById(MAX_ID)).thenReturn(null);

        result = countryService.getCountryById(MAX_ID);

        assertNull(result);
        verify(countryRepository, times(1)).findById(MAX_ID);
        verifyNoMoreInteractions(countryRepository);
    }

    //=============================================================================================================//

    @Test
    public void testGetCountryByCode() {
        Optional<Country> country;
        ExampleMatcher em = ExampleMatcher.matchingAny();
        Example<Country> countryExample = Example.of(new Country(), em);
        when(countryRepository.findOne( any( countryExample.getClass())) ).thenReturn(countryAOpt);

        country = countryService.getCountryByCode("AR", AppConstants.NOT_INCLUDE_DELETED);

        assertEquals(countryAOpt, country);
        verify(countryRepository, times(1)).findOne(any(countryExample.getClass()));
        verifyNoMoreInteractions(countryRepository);

    }

    @Test
    public void testGetCountryByCodeFail() {
        Optional<Country> country;
        ExampleMatcher em = ExampleMatcher.matchingAny();
        Example<Country> countryExample = Example.of(new Country(), em);
        when(countryRepository.findOne( any( countryExample.getClass())) ).thenReturn(Optional.ofNullable(null));

        country = countryService.getCountryByCode(MAX_CODE,AppConstants.NOT_INCLUDE_DELETED);

        assertEquals(country.isPresent(),false);
        verify(countryRepository, times(1)).findOne(any(countryExample.getClass()));
        verifyNoMoreInteractions(countryRepository);

    }

    //=============================================================================================================//

    @Test
    public void testCreateCountry() {

        Country c;
        when(countryRepository.save(any(Country.class))).thenReturn(countryA);

        c = countryService.createCountry(countryA);

        assertEquals(c.getId(), countryA.getId());
        verify(countryRepository, times(1)).save(countryA);
        verifyNoMoreInteractions(countryRepository);

    }

    @Test
    public void testCreateCountryFail() {

        when(countryRepository.save(null)).thenThrow(IllegalArgumentException.class);

        assertThrows(IllegalArgumentException.class, () -> countryService.createCountry(null));

        verify(countryRepository, times(1)).save(null);
        verifyNoMoreInteractions(countryRepository);

    }

    //=============================================================================================================//

    @Test
    public void testUpdateCountry() {

        Country c;
        when(countryRepository.save(any(Country.class))).thenReturn(countryA);

        c = countryService.updateCountry(countryA);

        assertEquals(c.getId(), countryA.getId());
        verify(countryRepository, times(1)).save(countryA);
        verifyNoMoreInteractions(countryRepository);

    }

    @Test
    public void testUpdateCountryFail() {

        when(countryRepository.save(null)).thenThrow(IllegalArgumentException.class);

        assertThrows(IllegalArgumentException.class, () -> countryService.updateCountry(null));

        verify(countryRepository, times(1)).save(null);
        verifyNoMoreInteractions(countryRepository);

    }

    //=============================================================================================================//

}
