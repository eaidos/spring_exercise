package com.conexia.restexercise;

import com.conexia.restexercise.constants.AppConstants;
import com.conexia.restexercise.dto.internal.response.WeatherResponseDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WeatherControllerIntegrationTest {

    private final String BASE_URI = "http://localhost:8080/restexercise/weather/current/";
    private final String BASE_URI_STATION = "http://localhost:8080/restexercise/weather/current/station/";
    private final String COORDINATES = "-34.605425/-58.381555";
    private final String COORDINATES_ERROR = "-200.A/-200.A";

    private final long MAX_ID = Long.MAX_VALUE;
    private final String MAX_CODE = "ZZZZ";
    private RestTemplate template;
    private ObjectMapper om;

    @Before
    public void init(){
        om = new ObjectMapper();
        om.registerModule(new JavaTimeModule());
        om.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        template = new RestTemplate();
    }

    //=============================================================================================================//

    @Test
    public void testGetWeatherByCoordinates() {
        ResponseEntity<WeatherResponseDTO> response = template.getForEntity(BASE_URI + COORDINATES,
                                                                            WeatherResponseDTO.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getData().isEmpty(),is(false));
        assertThat(response.getBody().getData().size(),is(1));
        assertThat(response.getBody().getStatus(),is(AppConstants.SUCCESS));
    }

    @Test
    public void testGetWeatherByCoordinatesFail() throws JsonProcessingException {
        try {
            ResponseEntity<WeatherResponseDTO> response = template.getForEntity(BASE_URI + COORDINATES_ERROR,
                                                                                WeatherResponseDTO.class);
        } catch (HttpClientErrorException e) {
            assertNotEquals(e.getStatusCode(), (HttpStatus.OK));
        }
    }
    //=============================================================================================================//

    @Test
    public void testGetWeatherByStation() {
        ResponseEntity<WeatherResponseDTO> response = template.getForEntity(BASE_URI_STATION + "SADP",
                                                                            WeatherResponseDTO.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getData().isEmpty(),is(false));
        assertThat(response.getBody().getData().size(),is(1));
        assertThat(response.getBody().getStatus(),is(AppConstants.SUCCESS));
    }

    @Test
    public void testGetWeatherByStationNotFound() throws JsonProcessingException {
        try {
            ResponseEntity<WeatherResponseDTO> response = template.getForEntity(BASE_URI_STATION + MAX_CODE,
                                                                                WeatherResponseDTO.class);
            assertNotEquals(response.getStatusCode(), (HttpStatus.OK));
            assertEquals(response.getBody().getData().size(), 0);
            assertEquals(response.getBody().getStatus(), AppConstants.NOT_FOUND);
        } catch (HttpClientErrorException e) {
            assertNotEquals(e.getStatusCode(), (HttpStatus.OK));
        } catch (HttpServerErrorException e) {
            assertNotEquals(e.getStatusCode(), (HttpStatus.OK));
        }
    }

    //=============================================================================================================//

    @Test
    public void testGetWeatherByPostalCode() {
        ResponseEntity<WeatherResponseDTO> response = template.getForEntity(BASE_URI + "1678",
                                                                            WeatherResponseDTO.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getData().isEmpty(),is(false));
        assertThat(response.getBody().getData().size(),is(1));
        assertThat(response.getBody().getStatus(),is(AppConstants.SUCCESS));
    }

    @Test
    public void testGetWeatherByPostalCodeNotFound() throws JsonProcessingException {
        try {
            ResponseEntity<WeatherResponseDTO> response = template.getForEntity(BASE_URI + MAX_ID,
                                                                                WeatherResponseDTO.class);
            assertNotEquals(response.getStatusCode(), (HttpStatus.OK));
            assertEquals(response.getBody().getData().size(), 0);
            assertEquals(response.getBody().getStatus(), AppConstants.NOT_FOUND);
        } catch (HttpClientErrorException e) {
            assertNotEquals(e.getStatusCode(), (HttpStatus.OK));
        } catch (HttpServerErrorException e) {
            assertNotEquals(e.getStatusCode(), (HttpStatus.OK));
        }
    }
    //=============================================================================================================//






}
