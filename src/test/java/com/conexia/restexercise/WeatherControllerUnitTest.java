package com.conexia.restexercise;

import com.conexia.restexercise.constants.AppConstants;
import com.conexia.restexercise.controller.CityController;
import com.conexia.restexercise.domain.Coordinates;
import com.conexia.restexercise.domain.Country;
import com.conexia.restexercise.domain.Weather;
import com.conexia.restexercise.domain.WeatherIcon;
import com.conexia.restexercise.domain.WeatherStation;
import com.conexia.restexercise.dto.external.request.WeatherByCoordinatesRequestDTO;
import com.conexia.restexercise.dto.external.request.WeatherByPostalRequestDTO;
import com.conexia.restexercise.dto.external.request.WeatherByStationRequestDTO;
import com.conexia.restexercise.dto.external.response.DailyWeatherResponseDTO;
import com.conexia.restexercise.dto.internal.response.WeatherResponseDTO;
import com.conexia.restexercise.exception.ExtWeatherServiceError;
import com.conexia.restexercise.exception.ExtWeatherServiceNoData;
import com.conexia.restexercise.service.CityService;
import com.conexia.restexercise.service.CountryService;
import com.conexia.restexercise.service.RequestService;
import com.conexia.restexercise.service.WeatherService;
import com.conexia.restexercise.service.WeatherStationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class WeatherControllerUnitTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RequestService requestService;

    @MockBean
    private CityService cityService;

    @MockBean
    private WeatherStationService weatherStationService;

    @MockBean
    private WeatherService weatherService;

    @MockBean
    private CountryService countryService;

    @InjectMocks
    private CityController cityController;

    private final long MAX_ID = Long.MAX_VALUE;
    private final String POSTAL_CODE = "/1678";
    private final String POSTAL_CODE_FAIL = "/16784234434";
    private final String STATION = "/SADP";
    private final String STATION_FAIL = "/ZZZZ";
    private final String WS_PATH = "/weather/current";
    private final String WS_PATH_STATION = "/weather/current/station";
    private final String LAT_LON = "/21/43";
    private final String LAT_LON_FAIL = "/2A1/4A3";
    private WeatherByCoordinatesRequestDTO wByCoord;
    private WeatherByStationRequestDTO wByStation;
    private WeatherByPostalRequestDTO wByPostal;

    private DailyWeatherResponseDTO dailyWeatherResponse;
    private List<Weather> weatherResponse;
    private Weather weather;
    private ObjectMapper om;
    private List<DailyWeatherResponseDTO> dailyWeatherResponseList;
    private ExtWeatherServiceError coordException;
    private ExtWeatherServiceNoData stationException;
    private ExtWeatherServiceNoData postalException;
    private Optional<WeatherStation> ws;
    private Country countryA;

    @Before
    public void init(){

        weather = new Weather("E", 60, "d", "43",
                              BigDecimal.valueOf(1012), "America/Argentina/Buenos_Aires",
                              "2020-04-21 16:37",
                              "AR", BigDecimal.valueOf(1), BigDecimal.valueOf(11), BigDecimal.valueOf(600),
                              "east", BigDecimal.valueOf(23), "01", 123,
                              BigDecimal.valueOf(1), BigDecimal.valueOf(1), new WeatherIcon("2","2","2"), 1, 1, "SADP",
                              BigDecimal.valueOf(101), BigDecimal.valueOf(1), "2020-04-21:16",
                              BigDecimal.valueOf(1), BigDecimal.valueOf(1), BigDecimal.valueOf(1),
                              BigDecimal.valueOf(1), BigDecimal.valueOf(1), "Caseros", "21:24:07",
                              "10:21:49", BigDecimal.valueOf(19.4), "21", BigDecimal.valueOf(32),
                              BigDecimal.valueOf(0));

        weatherResponse = Arrays.asList(weather);

        dailyWeatherResponse = new DailyWeatherResponseDTO(1, weatherResponse, AppConstants.SUCCESS);

        dailyWeatherResponseList = Arrays.asList(dailyWeatherResponse);

        coordException = new ExtWeatherServiceError("{\"error\": \"Invalid lat/lon supplied.\"}\\n",
                                                    400);

        stationException = new ExtWeatherServiceNoData(AppConstants.NOT_FOUND,402);
        postalException = new ExtWeatherServiceNoData(AppConstants.NOT_FOUND, 402);

        countryA = new Country(1, "AR", "Argentina",
                               false, LocalDate.now(), null, null);
        ws = Optional.of(new WeatherStation(1, new Coordinates("-34.6", "-58.6"),
                                       "El Palomar Aerodrome", "SADP",
                                        countryA, false, LocalDate.now(), null,
                                       null));

        wByCoord = new WeatherByCoordinatesRequestDTO("21","43");
        wByStation = new WeatherByStationRequestDTO("ASDP");
        wByPostal = new WeatherByPostalRequestDTO("1678");
        om = new ObjectMapper();
        om.registerModule(new JavaTimeModule());
        om.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    //=============================================================================================================//

    @Test
    public void testGetWeatherByCoordinates() throws Exception {

        WeatherResponseDTO weatherResponseDTO = new WeatherResponseDTO();
        weatherResponseDTO.setStatus(AppConstants.SUCCESS);
        weatherResponseDTO.setMessage(AppConstants.WEATHER_SERVICE_SUCCESS_MESSAGE);
        weatherResponseDTO.setData(weatherResponse);

        when(weatherService.getCurrentWeatherByCoordinates(any(wByCoord.getClass()))).thenReturn(dailyWeatherResponse);

        mockMvc.perform(get(WS_PATH+LAT_LON))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(weatherResponseDTO)));

        verify(weatherService, times(1)).getCurrentWeatherByCoordinates(any(wByCoord.getClass()));
        verifyNoMoreInteractions(weatherService);

    }

    @Test
    public void testGetWeatherByCoordinatesFail() throws Exception {

        WeatherResponseDTO weatherResponseDTO = new WeatherResponseDTO();
        weatherResponseDTO.setStatus(AppConstants.ERROR);
        weatherResponseDTO.setMessage(coordException.getErrorMessage());
        weatherResponseDTO.setData(new ArrayList<>());

        when(weatherService.getCurrentWeatherByCoordinates(any(wByCoord.getClass()))).thenThrow
                (coordException);

        mockMvc.perform(get(WS_PATH + LAT_LON_FAIL))
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(weatherResponseDTO)));

        verify(weatherService, times(1)).getCurrentWeatherByCoordinates(any(wByCoord.getClass()));
        verifyNoMoreInteractions(weatherService);

    }
    //=============================================================================================================//

    @Test
    public void testGetWeatherByStation() throws Exception {

        WeatherResponseDTO weatherResponseDTO = new WeatherResponseDTO();
        weatherResponseDTO.setStatus(AppConstants.SUCCESS);
        weatherResponseDTO.setMessage(AppConstants.WEATHER_SERVICE_SUCCESS_MESSAGE);
        weatherResponseDTO.setData(weatherResponse);

        when(weatherService.getCurrentWeatherByStation(any(wByStation.getClass()))).thenReturn(dailyWeatherResponse);
        when(weatherStationService.getWeatherStationByCode("SADP")).thenReturn(ws);

        mockMvc.perform(get(WS_PATH_STATION+STATION))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(weatherResponseDTO)));

        verify(weatherStationService, times(1)).getWeatherStationByCode("SADP");
        verify(weatherService, times(1)).getCurrentWeatherByStation(any(wByStation.getClass()));
        verifyNoMoreInteractions(weatherService);

    }

    @Test
    public void testGetWeatherByStationFail() throws Exception {

        WeatherResponseDTO weatherResponseDTO = new WeatherResponseDTO();
        weatherResponseDTO.setStatus(AppConstants.ERROR);
        weatherResponseDTO.setMessage(AppConstants.FIND_ALL_WEATHER_STATION_EMPTY_MESSAGE);
        weatherResponseDTO.setData(new ArrayList<>());

        when(weatherService.getCurrentWeatherByStation(any(wByStation.getClass()))).thenThrow(stationException);

        mockMvc.perform(get(WS_PATH_STATION + STATION_FAIL))
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(weatherResponseDTO)));

        verify(weatherStationService, times(1)).getWeatherStationByCode("ZZZZ");
        verifyNoMoreInteractions(weatherService);

    }
    //=============================================================================================================//

    @Test
    public void testGetWeatherByPostalCode() throws Exception {

        WeatherResponseDTO weatherResponseDTO = new WeatherResponseDTO();
        weatherResponseDTO.setStatus(AppConstants.SUCCESS);
        weatherResponseDTO.setMessage(AppConstants.WEATHER_SERVICE_SUCCESS_MESSAGE);
        weatherResponseDTO.setData(weatherResponse);

        when(weatherService.getCurrentWeatherByPostalCode(any(wByPostal.getClass()))).thenReturn(dailyWeatherResponse);

        mockMvc.perform(get(WS_PATH+POSTAL_CODE))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(weatherResponseDTO)));

        verify(weatherService, times(1)).getCurrentWeatherByPostalCode(any(wByPostal.getClass()));
        verifyNoMoreInteractions(weatherService);

    }

    @Test
    public void testGetWeatherByPostalCodeFail() throws Exception {

        WeatherResponseDTO weatherResponseDTO = new WeatherResponseDTO();
        weatherResponseDTO.setStatus(AppConstants.NOT_FOUND);
        weatherResponseDTO.setMessage(AppConstants.WEATHER_SERVICE_NO_DATA);
        weatherResponseDTO.setData(new ArrayList<>());

        when(weatherService.getCurrentWeatherByPostalCode(any(wByPostal.getClass()))).thenThrow(postalException);

        mockMvc.perform(get(WS_PATH+POSTAL_CODE_FAIL))
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(weatherResponseDTO)));

        verify(weatherService, times(1)).getCurrentWeatherByPostalCode(any(wByPostal.getClass()));
        verifyNoMoreInteractions(weatherService);

    }
    //=============================================================================================================//

}
