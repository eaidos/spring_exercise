package com.conexia.restexercise;

import com.conexia.restexercise.constants.AppConstants;
import com.conexia.restexercise.domain.Coordinates;
import com.conexia.restexercise.dto.internal.request.WeatherStationRequestDTO;
import com.conexia.restexercise.dto.internal.response.WeatherStationResponseDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedHashMap;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
public class WeatherStationControllerIntegrationTest {

    private final String BASE_URI = "http://localhost:8080/restexercise/weatherstation/";
    private final long MAX_ID = Long.MAX_VALUE;
    private final String MAX_CODE = "ZZZZ";
    private RestTemplate template;

    @Before
    public void init(){
        template = new RestTemplate();
    }

    //=============================================================================================================//

    @Test
    public void testGetAllWeatherStations() {
        ResponseEntity<WeatherStationResponseDTO> response = template.getForEntity(BASE_URI, WeatherStationResponseDTO.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getData().isEmpty(),is(false));
    }

    //=============================================================================================================//

    @Test
    public void testGetWeatherStationById() {

        ResponseEntity<WeatherStationResponseDTO> response = template.getForEntity(BASE_URI+"1",
                                                                                   WeatherStationResponseDTO.class);

        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getData().isEmpty(),is(false));
        val weatherStation = (LinkedHashMap<?,?>) response.getBody().getData().get(0);
        assertEquals(weatherStation.get("id"),1);
    }

    @Test
    public void testGetWeatherStationByIdNotFound() throws JsonProcessingException {
        try {
            ResponseEntity<WeatherStationResponseDTO> response = template.getForEntity(BASE_URI + MAX_CODE,
                                                                                WeatherStationResponseDTO.class);
        } catch (HttpClientErrorException e) {
            assertThat(e.getStatusCode(), is(HttpStatus.NOT_FOUND));
            ObjectMapper om = new ObjectMapper();
            WeatherStationResponseDTO responseDTO = om.readValue(e.getResponseBodyAsString(), WeatherStationResponseDTO.class);
            assertThat(responseDTO.getData().isEmpty(),is(true));
            assertEquals(responseDTO.getStatus(),AppConstants.ERROR);
        }
    }

    //=============================================================================================================//

    @Test
    public void testGetWeatherStationByCode() {
        ResponseEntity<WeatherStationResponseDTO> response = template.getForEntity(BASE_URI+"SADP",WeatherStationResponseDTO.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getData().isEmpty(),is(false));
    }

    @Test
    public void testGeWeatherStationByCodeNotFound() throws JsonProcessingException {
        try {
            ResponseEntity<WeatherStationResponseDTO> response = template.getForEntity(BASE_URI+MAX_CODE,
                                                                                WeatherStationResponseDTO.class);
        } catch (HttpClientErrorException e) {
            assertThat(e.getStatusCode(), is(HttpStatus.NOT_FOUND));
            ObjectMapper om = new ObjectMapper();
            WeatherStationResponseDTO responseDTO = om.readValue(e.getResponseBodyAsString(), WeatherStationResponseDTO.class);
            assertThat(responseDTO.getData().isEmpty(),is(true));
            assertEquals(responseDTO.getStatus(),AppConstants.ERROR);
        }
    }

    //=============================================================================================================//

    @Test
    public void testGetAllWeatherStationsByCountryId() {
        ResponseEntity<WeatherStationResponseDTO> response = template.getForEntity(BASE_URI + "country/1",
                                                                                   WeatherStationResponseDTO.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getData().isEmpty(),is(false));
    }

    @Test
    public void testGetAllWeatherStationsByCountryIdIdNotFound() throws JsonProcessingException {
        try {
            ResponseEntity<WeatherStationResponseDTO> response = template.getForEntity(BASE_URI + "country/" + MAX_ID,
                                                                                       WeatherStationResponseDTO.class);
        } catch (HttpClientErrorException e) {
            assertThat(e.getStatusCode(), is(HttpStatus.NOT_FOUND));
            ObjectMapper om = new ObjectMapper();
            WeatherStationResponseDTO responseDTO = om.readValue(e.getResponseBodyAsString(), WeatherStationResponseDTO.class);
            assertThat(responseDTO.getData().isEmpty(),is(true));
            assertEquals(responseDTO.getStatus(),AppConstants.ERROR);
        }
    }

    //=============================================================================================================//

    @Test
    public void testModifyWeatherStationByCode() {
        val weatherStationRequestDTO = new WeatherStationRequestDTO();
        weatherStationRequestDTO.setDescription("Test Weather Station");
        weatherStationRequestDTO.setCode("TEST");
        weatherStationRequestDTO.setCoordinates(new Coordinates("1","-1"));
        weatherStationRequestDTO.setCountryId(1L);
        template.put(BASE_URI+"SABE",weatherStationRequestDTO);
    }

    @Test
    public void testModifyWeatherStationByCodeNotFound() throws JsonProcessingException {
        val weatherStationRequestDTO = new WeatherStationRequestDTO();
        weatherStationRequestDTO.setCountryId(MAX_ID);
        try {
            template.put(BASE_URI+MAX_CODE,weatherStationRequestDTO);
        } catch (HttpClientErrorException e) {
            assertThat(e.getStatusCode(), is(HttpStatus.NOT_FOUND));
            ObjectMapper om = new ObjectMapper();
            WeatherStationResponseDTO responseDTO = om.readValue(e.getResponseBodyAsString(), WeatherStationResponseDTO.class);
            assertThat(responseDTO.getData().isEmpty(),is(true));
            assertEquals(responseDTO.getStatus(),AppConstants.ERROR);
        }
    }
    //=============================================================================================================//

    @Test
    public void testCreateWeatherStation() {
        val weatherStationRequestDTO = new WeatherStationRequestDTO();
        weatherStationRequestDTO.setDescription("Test Weather Station");
        weatherStationRequestDTO.setCode("TEST");
        weatherStationRequestDTO.setCoordinates(new Coordinates("1","-1"));
        weatherStationRequestDTO.setCountryId(1L);
        ResponseEntity<WeatherStationResponseDTO> response = template.postForEntity(BASE_URI, weatherStationRequestDTO,
                                                                             WeatherStationResponseDTO.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getData().isEmpty(),is(false));
    }

    @Test
    public void testCreateWeatherStationNotFound() throws JsonProcessingException {

        val weatherStationRequestDTO = new WeatherStationRequestDTO();
        weatherStationRequestDTO.setDescription("Test Weather Station");
        weatherStationRequestDTO.setCode("TEST2");
        weatherStationRequestDTO.setCoordinates(new Coordinates("1","-1"));
        weatherStationRequestDTO.setCountryId(MAX_ID);
        try {
            ResponseEntity<WeatherStationResponseDTO> response = template.postForEntity(BASE_URI, weatherStationRequestDTO,
                                                                                 WeatherStationResponseDTO.class);
        } catch (HttpClientErrorException e) {
            assertThat(e.getStatusCode(), is(HttpStatus.NOT_FOUND));
            ObjectMapper om = new ObjectMapper();
            WeatherStationResponseDTO responseDTO = om.readValue(e.getResponseBodyAsString(), WeatherStationResponseDTO.class);
            assertThat(responseDTO.getData().isEmpty(),is(true));
            assertEquals(responseDTO.getStatus(),AppConstants.ERROR);
        }
    }

    //=============================================================================================================//

    @Test
    public void testDeleteWeatherStationByCode() {
        template.delete(BASE_URI+"SAEZ");
    }

    @Test
    public void testDeleteWeatherStationByCodeNotFound() throws JsonProcessingException {
        try {
            template.delete(BASE_URI+MAX_CODE);
        } catch (HttpClientErrorException e) {
            assertThat(e.getStatusCode(), is(HttpStatus.NOT_FOUND));
            ObjectMapper om = new ObjectMapper();
            WeatherStationResponseDTO responseDTO = om.readValue(e.getResponseBodyAsString(), WeatherStationResponseDTO.class);
            assertThat(responseDTO.getData().isEmpty(),is(true));
            assertEquals(responseDTO.getStatus(),AppConstants.ERROR);
        }
    }

}