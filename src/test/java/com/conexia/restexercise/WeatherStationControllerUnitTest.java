package com.conexia.restexercise;

import com.conexia.restexercise.constants.AppConstants;
import com.conexia.restexercise.controller.WeatherStationController;
import com.conexia.restexercise.domain.Coordinates;
import com.conexia.restexercise.domain.Country;
import com.conexia.restexercise.domain.WeatherStation;
import com.conexia.restexercise.dto.internal.request.WeatherStationRequestDTO;
import com.conexia.restexercise.dto.internal.response.CityResponseDTO;
import com.conexia.restexercise.dto.internal.response.WeatherStationResponseDTO;
import com.conexia.restexercise.service.CityService;
import com.conexia.restexercise.service.CountryService;
import com.conexia.restexercise.service.WeatherStationService;
import com.conexia.restexercise.service.RequestService;
import com.conexia.restexercise.service.WeatherService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest
public class WeatherStationControllerUnitTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RequestService requestService;

    @MockBean
    private CityService cityService;

    @MockBean
    private WeatherStationService weatherStationService;

    @MockBean
    private WeatherService weatherService;

    @MockBean
    private CountryService countryService;

    @InjectMocks
    private WeatherStationController weatherStationController;

    private final long MAX_ID = Long.MAX_VALUE;
    private final String MAX_CODE = "ZZZZ";
    private final String WS_PATH = "/weatherstation/";
    private final String WS_COUNTRY_PATH = "/weatherstation/country/";


    private List<WeatherStation> weatherStationListWithOneDeleted;
    private List<WeatherStation> weatherStationListWithOneUpdated;
    private List<WeatherStation> weatherStationListWithTwo;
    private List<WeatherStation> weatherStationListWithOne;
    private List<WeatherStation> emptyWeatherStationList;
    private Optional<WeatherStation> weatherStationAOpt;
    private WeatherStation weatherStationA;
    private WeatherStation weatherStationC;
    private WeatherStation weatherStationCH;
    private ObjectMapper om;
    private WeatherStation weatherStationAUpdated;
    private Country countryA;
    private Country countryC;

    @Before
    public void init(){

        countryA = new Country(1, "AR", "Argentina",
                               false, LocalDate.now(), null, null);

        countryC = new Country(2, "CO", "Colombia",
                               false, LocalDate.now(),null,null);

        weatherStationA = new WeatherStation(1, new Coordinates("-34.6","-58.6"),
                                             "El Palomar Aerodrome","SADP",
                                             countryA, false,LocalDate.now(),null,null);

        weatherStationAOpt = Optional.of(new WeatherStation(1, new Coordinates("-34.6", "-58.6"),
                                                            "El Palomar Aerodrome", "SADP",
                                                            countryA, false, LocalDate.now(), null,
                                                            null));

        weatherStationC = new WeatherStation(2, new Coordinates("13.3667","-81.35"),
                                             "Providencia Isla / El Embrujo","SKPV",
                                             countryC, false,LocalDate.now(),null,null);

        weatherStationAUpdated = new WeatherStation(1, new Coordinates("99","99"),
                                             "Test weather station","TEST", countryC,
                                             false, LocalDate.now(),null, LocalDate.now());

        weatherStationListWithTwo = Arrays.asList(weatherStationA, weatherStationC);

        weatherStationListWithOneUpdated = Arrays.asList(weatherStationAUpdated);

        weatherStationListWithOne = Arrays.asList(weatherStationA);

        emptyWeatherStationList = Arrays.asList();

        om = new ObjectMapper();
        om.registerModule(new JavaTimeModule());
        om.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    //=============================================================================================================//

    @Test
    public void testGetAllWeatherStations() throws Exception {

        WeatherStationResponseDTO weatherStationResponseDTO = new WeatherStationResponseDTO();
        weatherStationResponseDTO.setStatus(AppConstants.SUCCESS);
        weatherStationResponseDTO.setMessage(AppConstants.FIND_ALL_WEATHER_STATION_SUCCESS_MESSAGE);
        weatherStationResponseDTO.setData(weatherStationListWithTwo);

        when(weatherStationService.getAllWeatherStations()).thenReturn(weatherStationListWithTwo);

        mockMvc.perform(get(WS_PATH))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(weatherStationResponseDTO)));

        verify(weatherStationService, times(1)).getAllWeatherStations();
        verifyNoMoreInteractions(weatherStationService);

    }

    @Test
    public void testGetAllWeatherStationsFail() throws Exception {

        WeatherStationResponseDTO weatherStationResponseDTO = new WeatherStationResponseDTO();
        weatherStationResponseDTO.setStatus(AppConstants.ERROR);
        weatherStationResponseDTO.setMessage(AppConstants.FIND_ALL_WEATHER_STATION_EMPTY_MESSAGE);
        weatherStationResponseDTO.setData(emptyWeatherStationList);

        when(weatherStationService.getAllWeatherStations()).thenReturn(emptyWeatherStationList);

        mockMvc.perform(get(WS_PATH))
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(weatherStationResponseDTO)));

        verify(weatherStationService, times(1)).getAllWeatherStations();
        verifyNoMoreInteractions(weatherStationService);

    }

    //=============================================================================================================//

    @Test
    public void testGetWeatherStationById() throws Exception {

        WeatherStationResponseDTO weatherStationResponseDTO = new WeatherStationResponseDTO();
        weatherStationResponseDTO.setStatus(AppConstants.SUCCESS);
        weatherStationResponseDTO.setMessage(AppConstants.FIND_WEATHER_STATION_SUCCESS_MESSAGE);
        weatherStationResponseDTO.setData(weatherStationListWithOne);

        when(weatherStationService.getWeatherStationById(1)).thenReturn(java.util.Optional.ofNullable(weatherStationA));

        mockMvc.perform(get(WS_PATH+"1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(weatherStationResponseDTO)));

        verify(weatherStationService, times(1)).getWeatherStationById(1);
        verifyNoMoreInteractions(weatherStationService);
    }

    @Test
    public void testGetWeatherStationByIdFail() throws Exception {

        WeatherStationResponseDTO weatherStationResponseDTO = new WeatherStationResponseDTO();
        weatherStationResponseDTO.setStatus(AppConstants.ERROR);
        weatherStationResponseDTO.setMessage(AppConstants.FIND_WEATHER_STATION_EMPTY_MESSAGE);
        weatherStationResponseDTO.setData(emptyWeatherStationList);

        when(weatherStationService.getWeatherStationById(MAX_ID)).thenReturn(java.util.Optional.ofNullable(null));

        mockMvc.perform(get(WS_PATH+MAX_ID))
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(weatherStationResponseDTO)));

        verify(weatherStationService, times(1)).getWeatherStationById(MAX_ID);
        verifyNoMoreInteractions(weatherStationService);
    }

    //=============================================================================================================//

    @Test
    public void testGetWeatherStationByCode() throws Exception {
        WeatherStationResponseDTO weatherStationResponseDTO = new WeatherStationResponseDTO();
        weatherStationResponseDTO.setStatus(AppConstants.SUCCESS);
        weatherStationResponseDTO.setMessage(AppConstants.FIND_WEATHER_STATION_SUCCESS_MESSAGE);
        weatherStationResponseDTO.setData(weatherStationListWithOne);

        when(weatherStationService.getWeatherStationByCode("SADP")).thenReturn(weatherStationAOpt);

        mockMvc.perform(get(WS_PATH+"SADP"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(weatherStationResponseDTO)));

        verify(weatherStationService, times(1)).getWeatherStationByCode("SADP");
        verifyNoMoreInteractions(weatherStationService);
    }

    @Test
    public void testGetWeatherStationByCodeFail() throws Exception {
        WeatherStationResponseDTO weatherStationResponseDTO = new WeatherStationResponseDTO();
        weatherStationResponseDTO.setStatus(AppConstants.ERROR);
        weatherStationResponseDTO.setMessage(AppConstants.FIND_WEATHER_STATION_EMPTY_MESSAGE);
        weatherStationResponseDTO.setData(emptyWeatherStationList);

        when(weatherStationService.getWeatherStationByCode(MAX_CODE)).thenReturn(Optional.ofNullable(null));

        mockMvc.perform(get(WS_PATH+MAX_CODE))
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(weatherStationResponseDTO)));

        verify(weatherStationService, times(1)).getWeatherStationByCode(MAX_CODE);
        verifyNoMoreInteractions(weatherStationService);
    }

    //=============================================================================================================//

    @Test
    public void testGetAllWeatherStationsByCountryId() throws Exception {
        WeatherStationResponseDTO weatherStationResponseDTO = new WeatherStationResponseDTO();
        weatherStationResponseDTO.setStatus(AppConstants.SUCCESS);
        weatherStationResponseDTO.setMessage(AppConstants.FIND_ALL_WEATHER_STATION_SUCCESS_MESSAGE);
        weatherStationResponseDTO.setData(weatherStationListWithOne);

        when(weatherStationService.getAllStationsByCountryId(1)).thenReturn(weatherStationListWithOne);
        when(countryService.getCountryById(1)).thenReturn(java.util.Optional.ofNullable(countryA));

        mockMvc.perform(get(WS_COUNTRY_PATH+1))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(weatherStationResponseDTO)));

        verify(weatherStationService, times(1)).getAllStationsByCountryId(1);
        verify(countryService, times(1)).getCountryById(1);
        verifyNoMoreInteractions(cityService);
    }

    @Test
    public void testGetAllWeatherStationsByCountryIdFail() throws Exception {
        CityResponseDTO cityResponseDTO = new CityResponseDTO();
        cityResponseDTO.setStatus(AppConstants.ERROR);
        cityResponseDTO.setMessage(AppConstants.FIND_COUNTRY_EMPTY_MESSAGE);
        cityResponseDTO.setData(emptyWeatherStationList);

        when(weatherStationService.getAllStationsByCountryId(MAX_ID)).thenReturn(emptyWeatherStationList);

        mockMvc.perform(get(WS_COUNTRY_PATH+MAX_ID))
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(cityResponseDTO)));

        verify(countryService, times(1)).getCountryById(MAX_ID);
        verifyNoMoreInteractions(cityService);
    }

    //=============================================================================================================//

    @Test
    public void createWeatherStation() throws Exception {
        WeatherStationResponseDTO weatherStationResponseDTO = new WeatherStationResponseDTO();
        weatherStationResponseDTO.setStatus(AppConstants.SUCCESS);
        weatherStationResponseDTO.setMessage(AppConstants.CREATE_WEATHER_STATION_SUCCESS_MESSAGE);
        weatherStationResponseDTO.setData(weatherStationListWithOne);

        WeatherStationRequestDTO request = new WeatherStationRequestDTO();
        request.setDescription(weatherStationA.getDescription());
        request.setCode(weatherStationA.getCode());
        request.setCoordinates(weatherStationA.getCoordinates());
        request.setCountryId(weatherStationA.getCountry().getId());

        when(weatherStationService.createWeatherStation(any())).thenReturn(weatherStationA);
        when(countryService.getCountryById(request.getCountryId())).thenReturn(Optional.ofNullable(countryA));


        mockMvc.perform(post(WS_PATH)
                                .content(om.writeValueAsString(request))
                                .contentType(MediaType.APPLICATION_JSON)
        )
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(weatherStationResponseDTO)));

        verify(countryService, times(1)).getCountryById(request.getCountryId());
        verify(weatherStationService, times(1)).createWeatherStation(any());
        verifyNoMoreInteractions(weatherStationService);
    }

    @Test
    public void createWeatherStationFail() throws Exception {
        WeatherStationResponseDTO weatherStationResponseDTO = new WeatherStationResponseDTO();
        weatherStationResponseDTO.setStatus(AppConstants.ERROR);
        weatherStationResponseDTO.setMessage(AppConstants.CREATE_WEATHER_STATION_ERROR_MESSAGE);
        weatherStationResponseDTO.setData(emptyWeatherStationList);

        WeatherStationRequestDTO request = new WeatherStationRequestDTO();
        request.setDescription(null);
        request.setCode(null);

        when(weatherStationService.createWeatherStation(any())).thenThrow(IllegalArgumentException.class);

        mockMvc.perform(post(WS_PATH)
                                .content(om.writeValueAsString(request))
                                .contentType(MediaType.APPLICATION_JSON)
        )
               .andExpect(status().is5xxServerError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(weatherStationResponseDTO)));

        verify(weatherStationService, times(0)).createWeatherStation(any());
        verifyNoMoreInteractions(weatherStationService);
    }

    //=============================================================================================================//

    @Test
    public void testModifyWeatherStationByCode() throws Exception {
        WeatherStationResponseDTO weatherStationResponseDTO = new WeatherStationResponseDTO();
        weatherStationResponseDTO.setStatus(AppConstants.SUCCESS);
        weatherStationResponseDTO.setMessage(AppConstants.UPDATE_WEATHER_STATION_SUCCESS_MESSAGE);
        weatherStationResponseDTO.setData(weatherStationListWithOneUpdated);

        WeatherStationRequestDTO request = new WeatherStationRequestDTO();
        request.setDescription("Test weather station");
        request.setCoordinates(new Coordinates("99","99"));
        request.setCountryId(2L);
        request.setCode("TEST");

        when(weatherStationService.getWeatherStationByCode("SADP"))
                                  .thenReturn(java.util.Optional.ofNullable(weatherStationA));
        when(countryService.getCountryById(2L)).thenReturn(Optional.ofNullable(countryC));
        when(weatherStationService.updateWeatherStation(weatherStationA)).thenReturn(weatherStationAUpdated);

        mockMvc.perform(put(WS_PATH+"SADP")
                                .content(om.writeValueAsString(request))
                                .contentType(MediaType.APPLICATION_JSON)
        )
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(weatherStationResponseDTO)));

        verify(weatherStationService, times(1)).getWeatherStationByCode("SADP");
        verify(weatherStationService, times(1)).updateWeatherStation(weatherStationA);
        verify(countryService, times(1)).getCountryById(2L);
        verifyNoMoreInteractions(weatherStationService);
    }

    @Test
    public void testModifyWeatherStationByCodeFail() throws Exception {
        WeatherStationResponseDTO weatherStationResponseDTO = new WeatherStationResponseDTO();
        weatherStationResponseDTO.setStatus(AppConstants.ERROR);
        weatherStationResponseDTO.setMessage(AppConstants.FIND_COUNTRY_EMPTY_MESSAGE);
        weatherStationResponseDTO.setData(emptyWeatherStationList);

        WeatherStationRequestDTO request = new WeatherStationRequestDTO();
        request.setCode(MAX_CODE);
        request.setCountryId(MAX_ID);

        when(weatherStationService.getWeatherStationByCode(MAX_CODE)).thenReturn(java.util.Optional.ofNullable(null));
        when(countryService.getCountryById(MAX_ID)).thenReturn(java.util.Optional.ofNullable(null));


        mockMvc.perform(put(WS_PATH+MAX_CODE)
                                .content(om.writeValueAsString(request))
                                .contentType(MediaType.APPLICATION_JSON)
        )
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(weatherStationResponseDTO)));

        verify(countryService, times(1)).getCountryById(MAX_ID);
        verify(weatherStationService, times(0)).updateWeatherStation(any());
        verifyNoMoreInteractions(weatherStationService);
    }

    //=============================================================================================================//

    @Test
    public void testDeleteWeatherStationByCode() throws Exception {
        WeatherStationResponseDTO weatherStationResponseDTO = new WeatherStationResponseDTO();
        weatherStationResponseDTO.setStatus(AppConstants.SUCCESS);
        weatherStationResponseDTO.setMessage(AppConstants.DELETE_WEATHER_STATION_SUCCESS_MESSAGE);
        weatherStationResponseDTO.setData(weatherStationListWithOne);

        when(weatherStationService.getWeatherStationByCode(weatherStationA.getCode()))
                .thenReturn(java.util.Optional.ofNullable(weatherStationA));
        //when(weatherStationService.deleteWeatherStation(weatherStationA)).thenReturn();

        mockMvc.perform(delete(WS_PATH+weatherStationA.getCode()) )
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(weatherStationResponseDTO)));

        verify(weatherStationService, times(1)).getWeatherStationByCode(weatherStationA.getCode());
        verify(weatherStationService, times(1)).deleteWeatherStation(weatherStationA);
        verifyNoMoreInteractions(weatherStationService);

    }

    @Test
    public void testDeleteWeatherStationByCodeFail() throws Exception {
        WeatherStationResponseDTO weatherStationResponseDTO = new WeatherStationResponseDTO();
        weatherStationResponseDTO.setStatus(AppConstants.ERROR);
        weatherStationResponseDTO.setMessage(AppConstants.FIND_WEATHER_STATION_EMPTY_MESSAGE);
        weatherStationResponseDTO.setData(emptyWeatherStationList);

        when(weatherStationService.getWeatherStationByCode(MAX_CODE)).thenReturn(java.util.Optional.ofNullable(null));

        mockMvc.perform(get(WS_PATH+MAX_CODE))
               .andExpect(status().is4xxClientError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(om.writeValueAsString(weatherStationResponseDTO)));

        verify(weatherStationService, times(1)).getWeatherStationByCode(MAX_CODE);
        verify(weatherStationService, times(0)).updateWeatherStation(any());
        verifyNoMoreInteractions(weatherStationService);

    }
}
