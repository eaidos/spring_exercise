package com.conexia.restexercise;

import com.conexia.restexercise.domain.Coordinates;
import com.conexia.restexercise.domain.Country;
import com.conexia.restexercise.domain.WeatherStation;
import com.conexia.restexercise.repository.IWeatherStationRepository;
import com.conexia.restexercise.service.WeatherStationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;


import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WeatherStationServiceTest {

    @Mock
    IWeatherStationRepository weatherStationRepository;

    @InjectMocks
    private WeatherStationService weatherStationService;

    private final long MAX_ID = Long.MAX_VALUE;
    private final String MAX_CODE = "ZZZZ";

    private List<WeatherStation> weatherStationListWithTwo;
    private List<WeatherStation> emptyWeatherStationList;
    private Optional<WeatherStation> weatherStationAOpt;
    private WeatherStation weatherStationA;
    private Country countryA;
    private Country countryB;
    private Country countryC;
    private List<WeatherStation> weatherStationListWithOne;
    private WeatherStation weatherStationC;

    @Before
    public void init(){
        countryA = new Country(1, "AR", "Argentina",
                               false, LocalDate.now(), null, null);
        countryB = new Country(99, "CN", "China",
                               false, LocalDate.now(),null,null);
        countryC = new Country(2, "CO", "Colombia",
                               false, LocalDate.now(),null,null);

        weatherStationA = new WeatherStation(1, new Coordinates("-34.6", "-58.6"),
                                             "El Palomar Aerodrome","SADP",
                                             countryA, false,LocalDate.now(),null,null);

        weatherStationAOpt = Optional.of(new WeatherStation(1, new Coordinates("-34.6", "-58.6"),
                                                            "El Palomar Aerodrome", "SADP",
                                                            countryA, false, LocalDate.now(), null,
                                                            null));

        weatherStationC = new WeatherStation(2, new Coordinates("13.3667","-81.35"),
                                             "Providencia Isla / El Embrujo","SKPV",
                                             countryC, false,LocalDate.now(),null,null);

        weatherStationListWithTwo = Arrays.asList(weatherStationA, weatherStationC);
        weatherStationListWithOne = Arrays.asList(weatherStationA);
        emptyWeatherStationList = Arrays.asList();

    }
    //=============================================================================================================//

    @Test
    public void testGetAllWeatherStations() {
        List<WeatherStation> weatherStationList;
        when(weatherStationRepository.findAll()).thenReturn(weatherStationListWithTwo);

        weatherStationList = weatherStationService.getAllWeatherStations();

        assertEquals(weatherStationList.size(),2);
        assertEquals(weatherStationList.get(0).getId(), 1);
        verify(weatherStationRepository, times(1)).findAll();
        verifyNoMoreInteractions(weatherStationRepository);
    }

    //=============================================================================================================//

    @Test
    public void testGetWeatherStationById() {
        Optional<WeatherStation> result;
        when(weatherStationRepository.findById(1L)).thenReturn(weatherStationAOpt);

        result = weatherStationService.getWeatherStationById(1);

        assertEquals(weatherStationAOpt, result);
        verify(weatherStationRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(weatherStationRepository);
    }

    @Test
    public void testGetWeatherStationByIdFail() {
        Optional<WeatherStation> result;
        when(weatherStationRepository.findById(MAX_ID)).thenReturn(null);

        result = weatherStationService.getWeatherStationById(MAX_ID);

        assertNull(result);
        verify(weatherStationRepository, times(1)).findById(MAX_ID);
        verifyNoMoreInteractions(weatherStationRepository);
    }

    //=============================================================================================================//

    @Test
    public void testGetWeatherStationByCode() {
        Optional<WeatherStation> weatherStation;
        ExampleMatcher em = ExampleMatcher.matchingAny();
        Example<WeatherStation> weatherStationExample = Example.of(new WeatherStation(), em);
        when(weatherStationRepository.findOne( any( weatherStationExample.getClass())) ).thenReturn(weatherStationAOpt);

        weatherStation = weatherStationService.getWeatherStationByCode("SADP");

        assertEquals(weatherStationAOpt, weatherStation);
        verify(weatherStationRepository, times(1)).findOne(any(weatherStationExample.getClass()));
        verifyNoMoreInteractions(weatherStationRepository);

    }

    @Test
    public void testGetWeatherStationByCodeFail() {
        Optional<WeatherStation> weatherStation;
        ExampleMatcher em = ExampleMatcher.matchingAny();
        Example<WeatherStation> weatherStationExample = Example.of(new WeatherStation(), em);
        when(weatherStationRepository.findOne( any( weatherStationExample.getClass())) ).thenReturn(Optional.ofNullable(null));

        weatherStation = weatherStationService.getWeatherStationByCode(MAX_CODE);

        assertEquals(weatherStation.isPresent(),false);
        verify(weatherStationRepository, times(1)).findOne(any(weatherStationExample.getClass()));
        verifyNoMoreInteractions(weatherStationRepository);

    }

    //=============================================================================================================//

    @Test
    public void testCreateWeatherStation() {

        WeatherStation ws;
        when(weatherStationRepository.save(any(WeatherStation.class))).thenReturn(weatherStationA);

        ws = weatherStationService.createWeatherStation(weatherStationA);

        assertEquals(ws.getId(), weatherStationA.getId());
        verify(weatherStationRepository, times(1)).save(weatherStationA);
        verifyNoMoreInteractions(weatherStationRepository);

    }

    @Test
    public void testCreateWeatherStationFail() {

        when(weatherStationRepository.save(null)).thenThrow(IllegalArgumentException.class);

        assertThrows(IllegalArgumentException.class, () -> weatherStationService.createWeatherStation(null));

        verify(weatherStationRepository, times(1)).save(null);
        verifyNoMoreInteractions(weatherStationRepository);

    }

    //=============================================================================================================//

    @Test
    public void testUpdateWeatherStation() {

        WeatherStation ws;
        when(weatherStationRepository.save(any(WeatherStation.class))).thenReturn(weatherStationA);

        ws = weatherStationService.updateWeatherStation(weatherStationA);

        assertEquals(ws.getId(), weatherStationA.getId());
        verify(weatherStationRepository, times(1)).save(weatherStationA);
        verifyNoMoreInteractions(weatherStationRepository);

    }

    @Test
    public void testUpdateWeatherStationFail() {

        when(weatherStationRepository.save(null)).thenThrow(IllegalArgumentException.class);

        assertThrows(IllegalArgumentException.class, () -> weatherStationService.createWeatherStation(null));

        verify(weatherStationRepository, times(1)).save(null);
        verifyNoMoreInteractions(weatherStationRepository);

    }

    //=============================================================================================================//

    @Test
    public void testDeleteWeatherStation() {

        doNothing().when(weatherStationRepository).delete(any(WeatherStation.class));

        weatherStationService.deleteWeatherStation(weatherStationA);

        verify(weatherStationRepository, times(1)).delete(weatherStationA);
        verifyNoMoreInteractions(weatherStationRepository);

    }

    @Test
    public void testDeleteWeatherStationFail() {

        doNothing().when(weatherStationRepository).delete(null);

        weatherStationService.deleteWeatherStation(null);

        verify(weatherStationRepository, times(1)).delete(null);
        verifyNoMoreInteractions(weatherStationRepository);

    }

    //=============================================================================================================//

    @Test
    public void testgetAllStationsByCountryId() {
        List<WeatherStation> weatherStationList;
        WeatherStation ws = new WeatherStation();
        ws.setCountry(countryA);
        ExampleMatcher em = ExampleMatcher.matchingAll().withIgnorePaths("id","description","coordinates",
                                                                         "dateCreated","dateDeleted","dateUpdated");
        Example<WeatherStation> wsExample = Example.of(ws, em);
        when(weatherStationRepository.findAll(any(wsExample.getClass()))).thenReturn(weatherStationListWithOne);

        weatherStationList = weatherStationService.getAllStationsByCountryId(countryA.getId());

        assertEquals(weatherStationList.size(),1);
        assertEquals(weatherStationList.get(0).getId(), countryA.getId());
        verify(weatherStationRepository, times(1)).findAll(any(wsExample.getClass()));
        verifyNoMoreInteractions(weatherStationRepository);

    }

    @Test
    public void testgetAllStationsByCountryIdFail() {
        List<WeatherStation> weatherStationList;
        WeatherStation ws = new WeatherStation();
        ws.setCountry(countryB);
        ExampleMatcher em = ExampleMatcher.matchingAll().withIgnorePaths("id","description","coordinates",
                                                                         "dateCreated","dateDeleted","dateUpdated");
        Example<WeatherStation> wsExample = Example.of(ws, em);

        when(weatherStationRepository.findAll(any(wsExample.getClass()))).thenReturn(emptyWeatherStationList);

        weatherStationList = weatherStationService.getAllStationsByCountryId(countryB.getId());

        assertEquals(weatherStationList.size(),0);
        verify(weatherStationRepository, times(1)).findAll(any(wsExample.getClass()));
        verifyNoMoreInteractions(weatherStationRepository);
    }

    //=============================================================================================================//

    @Test
    public void testGetAllStationsByCountryCode() {
        List<WeatherStation> weatherStationList;
        WeatherStation ws = new WeatherStation();
        ws.setCountry(countryA);
        ExampleMatcher em = ExampleMatcher.matchingAll().withIgnorePaths("id","description","coordinates",
                                                                         "dateCreated","dateDeleted","dateUpdated");
        Example<WeatherStation> wsExample = Example.of(ws, em);
        when(weatherStationRepository.findAll(any(wsExample.getClass()))).thenReturn(weatherStationListWithOne);

        weatherStationList = weatherStationService.getAllStationsByCountryCode(countryA.getCode());

        assertEquals(weatherStationList.size(),1);
        assertEquals(weatherStationList.get(0).getCode(), weatherStationA.getCode());
        verify(weatherStationRepository, times(1)).findAll(any(wsExample.getClass()));
        verifyNoMoreInteractions(weatherStationRepository);
    }

    @Test
    public void testGetAllStationsByCountryCodeFail() {
        List<WeatherStation> weatherStationList;
        WeatherStation ws = new WeatherStation();
        ws.setCountry(countryB);
        ExampleMatcher em = ExampleMatcher.matchingAll().withIgnorePaths("id","description","coordinates",
                                                                         "dateCreated","dateDeleted","dateUpdated");
        Example<WeatherStation> wsExample = Example.of(ws, em);

        when(weatherStationRepository.findAll(any(wsExample.getClass()))).thenReturn(emptyWeatherStationList);

        weatherStationList = weatherStationService.getAllStationsByCountryCode(countryB.getCode());

        assertEquals(weatherStationList.size(),0);
        verify(weatherStationRepository, times(1)).findAll(any(wsExample.getClass()));
        verifyNoMoreInteractions(weatherStationRepository);
    }

    //=============================================================================================================//
}